/* ================= Navigation Toggle Starts ================ */
$(document).ready(function(){
    $(".menu-toggle").click(function(){
        $('nav').toggleClass('active');
    });
});
/* ================= Navigation Toggle Ends ================ */

/* ================= Hidden Email js ================ */
var condition = false;
    $(document).ready(function(){
        $(".check").click(function(){
            if (!condition) {
                 $(".mail").css({'display':'inline'});
            }
            else{
                $(".mail").css({'display':'none'});
                        }
                 condition=!condition;
                    })
                });
/* ================= Hidden Email js Ends ================ */

/* ============= Type Only Numbers JS ============= */
function isInputNumber(evt){    
    var ch = String.fromCharCode(evt.which);
        if(!(/[0-9]/.test(ch))){
        evt.preventDefault();
        } 
    }          
/* ============= Type Only Numbers JS ends ============= */


/*---------------- FOR AMOUNT ------------------*/
$('select.fee-type').each(function () {
    switch ($(this).val()) {
        case '0':
            $(this).parent().parent().find('.fixed-rate').removeClass('hidden');
            $(this).parent().parent().find('.percentage').addClass('hidden');
            $(this).parent().parent().find('.percent').addClass('currency').removeClass('percent , input-group-text');
            $(this).parent().parent().find('.beneficiary-amount label').text('Amount Payable');
            break;
        case '1':
            $(this).parent().parent().find('.fixed-rate').addClass('hidden');
            $(this).parent().parent().find('.percentage').removeClass('hidden');
            $(this).parent().parent().find('.currency').addClass('percent').removeClass('currency');
            $(this).parent().parent().find('.beneficiary-amount label').text('Percentage of Transaction');
            break;
    }
});


/*---------------- FOR PERCENTAGE ------------------*/
$(document).on('change', 'select.fee-type', function () {
    switch ($(this).val()) {
        case '0':
            $(this).parent().parent().find('.fixed-rate').removeClass('hidden');
            $(this).parent().parent().find('.percentage').addClass('hidden');
            $(this).parent().parent().find('.percent').addClass('currency').removeClass('percent , input-group-text');
            $(this).parent().parent().find('.beneficiary-amount label').text('Amount Payable');
            break;
        case '1':
            $(this).parent().parent().find('.fixed-rate').addClass('hidden');
            $(this).parent().parent().find('.percentage').removeClass('hidden');
            $(this).parent().parent().find('.currency').addClass('percent').removeClass('currency');
            $(this).parent().parent().find('.beneficiary-amount label').text('Percentage of Transaction');
            break;
    }
});
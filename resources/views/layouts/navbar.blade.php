<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script> window.Laravel = { csrfToken: '{{csrf_token()}}'}</script>
    @yield('title')

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!----------------------------------Font Awesome CDN------------------------------------------------------------>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-----------------------  Favicon  -------------------->
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">

    <!-------------------Google Fonts----------------->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <!-- Styles -->
{{--    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
          integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
    <!---------------  Header ------------------>
    <header>
        <div class="logo"><a href=""><img src="{{asset('img/SafeTransact_logo.png')}}"></a></div>
        <nav>
            <ul>
                <li><a href="" class=" ">Home</a></li>
                <li><a href="">Start Transaction</a></li>
                <li><a href="">Analytics</a></li>
                @guest
                    <li><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
                    <li><a href="{{ route('register') }}">{{ __('Register') }}</a></li>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if(Auth::user()->first_name == null){{ Auth::user()->email }}@else{{ Auth::user()->first_name }} @endif<span class="caret"></span>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </nav>
        <div class="menu-toggle">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </div>
    </header>
    <!---------------  Header Ends------------------>

    @yield('content')

    <footer class="#">
        <!-- Footer Links -->
        <div class="container text-center text-md-left Hologram_style">
            <div class="row">
                <div class="col-md-6 ">
                    <div class="Content_A">
                        <h3 class="text-uppercase">SafeTransact</h3>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of
                            using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here',
                            making it look like readable English.
                        </p>
                    </div>
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-3">
                    <div id="contact-info" class="Content_B">
                        <address>
                            <strong>Headquarters:</strong><br>
                            <p>85th Agyei Taku Avenue,<br>Rodney Villa 1127 <br> Osu-Alata, Accra</p>
                        </address>
                    </div>
                </div>

                <div class="col-md-3">
                    <div id="phone-fax-email" class="Content_C">
                        <p>
                            <strong>Phone:</strong><span> +233 24 000 0000</span><br>
                            <strong>Fax:</strong><span> +233 50 000 0000</span><br>
                            <strong>Email:</strong><span> info@safetransact.com</span><br>
                        </p>
                    </div>
                    <ul class="list-social">
                        <li><a href="#" class="social-icon icon-white"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="social-icon icon-white"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="social-icon icon-white"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#" class="social-icon icon-white"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Footer Links -->
    </footer>

    <div id="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div id="footer-copyrights">
                        © 2019 Copyright:<a href="#"> SafeTransact</a>
                    </div>
                </div>
                <div class="col-md-6 hidden-sm hidden-xs">
                    <div id="footer-menu">
                        <ul>
                            <li><a class="smooth-scroll" href="#">Home</a>|</li>
                            <li><a class="smooth-scroll" href="#">Start Transaction</a>|</li>
                            <li><a class="smooth-scroll" href="#">Analytics</a></li>
                            @guest
                            <li>|<a class="smooth-scroll" href="{{ route('login') }}">Log In</a>|</li>
                            <li><a class="smooth-scroll" href="{{ route('register') }}">Sign Up</a></li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

    <!----- Popper Js ---->
    {{--<script src="js/popper.min.js"></script>--}}

    <!-- Popper Js CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

    <!-- Bootstrap js -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
            integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>

    <!-- Custom js -->
    <script src="{{asset('js/custom.js')}}"></script>

    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function(){--}}
            {{--$("#popoverButton_open").popover({container: 'body'});--}}
        {{--})--}}
    {{--</script>--}}

    @yield('scripts')
</body>
</html>

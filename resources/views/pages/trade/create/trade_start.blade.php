@extends('layouts.navbar')

@section('title')
    <title>Create New Trade</title>

    <link href="{{ asset('css/NewTrade.css') }}" rel="stylesheet">
    <link href="{{ asset('css/set_trade_parties.css') }}" rel="stylesheet">
    <link href="{{ asset('css/DescribeTrade.css') }}" rel="stylesheet">
    <link href="{{ asset('css/DescribeTradeAgent.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ProgressPayment.css') }}" rel="stylesheet">

@endsection

@section('content')
    <div id="create_trade">
        <trade-creation :auth_user="{{auth()->user()}}"
                        :fee_schedule="{{$feeSchedule}}"
                        :fee_type="{{$feeType}}"
                        :standard_fee_allocation="{{$standardFeeAllocation}}"
                        :admin_fee_allocation_agent="{{$adminFeeAllocationAgent}}"
                        :admin_fee_allocation="{{$adminFeeAllocation}}"
                        :beneficiary_type="{{$beneficiaryType}}"
                        :industry="{{$industry}}"></trade-creation>
    </div>
@endsection

@section('scripts')
    <!--------- Popover js ------------>
    <script>
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();
        })
    </script>
    <!--------- Popover js Ends------------>
    <script src="{{asset('js/app.js')}}"></script>
@endsection
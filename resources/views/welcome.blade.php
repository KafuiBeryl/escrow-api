@extends('layouts.navbar')

@section('title')
    <title>Welcome to SafeTransact</title>
@endsection

@section('content')
    <!------------------Banner Area ------------------>
    <section class="banner_area text-center">
        <div class="banner_inside">
            <h1>Welcome to SafeTransact</h1>
            <p>Ghana’s Trusted Online Escrow Service. From Buying or Selling a Phone to a Multi-Million Commodity Deal,
                <br>we Ensure That Every Transaction is Safe, Secure and Successful!</p>
            <div class="button-awesome">
                @auth
                    <a href="{{route('trade.start')}}" class="click-half">Start a new trade</a>
                @else
                <a href="{{ route('login') }}" class="click-half">Login to create a trade</a>
                @endauth
                <a href="" class="click-full">Learn More</a>
            </div>
        </div>
    </section>
    <!------------------Banner Area Ends------------------>
@endsection
{{--<!doctype html>--}}
{{--<html lang="{{ app()->getLocale() }}">--}}
    {{--<head>--}}
    {{--</head>--}}
    {{--<body>--}}
        {{--<div class="flex-center position-ref full-height">--}}
            {{--@if (Route::has('login'))--}}
                {{--<div class="top-right links">--}}
                    {{--@auth--}}
                        {{--<a href="{{ url('/home') }}">Home</a>--}}
                    {{--@else--}}
                        {{--<a href="{{ route('login') }}">Login</a>--}}
                        {{--<a href="{{ route('register') }}">Register</a>--}}
                    {{--@endauth--}}
                {{--</div>--}}
            {{--@endif--}}

            {{--<div class="content">--}}
                {{--<div class="title m-b-md">--}}
                    {{--Laravel--}}
                {{--</div>--}}

                {{--<div class="links">--}}
                    {{--<a href="https://laravel.com/docs">Documentation</a>--}}
                    {{--<a href="https://laracasts.com">Laracasts</a>--}}
                    {{--<a href="https://laravel-news.com">News</a>--}}
                    {{--<a href="https://forge.laravel.com">Forge</a>--}}
                    {{--<a href="https://github.com/laravel/laravel">GitHub</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</body>--}}
{{--</html>--}}

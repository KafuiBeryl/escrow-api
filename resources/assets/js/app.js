
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./jquery-3.3.1.min');
// require('./bootstrap.min');
// require('./popper.min');
// require('./dropzone/dropzone');
// require('./custom');

import Vue from 'vue';
import Vuex from 'vuex';
window.Vue = require('vue');
import BootstrapVue from "bootstrap-vue"
import "bootstrap-vue/dist/bootstrap-vue.css"
import Vuelidate from "vuelidate"
// window.$ = require('jquery');
// window.JQuery = require('jquery')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('trade-creation', require('./components/TradeCreation'));
// Vue.component('trade-role', require('./components/TradeRole.vue'));
// Vue.component('trade-party-buyer', require('./components/TradePartyBuyer.vue'));
// Vue.component('trade-party-seller', require('./components/TradePartySeller.vue'));
// Vue.component('trade-party-agent', require('./components/TradePartyAgent'));
// Vue.component('trade-description', require('./components/TradeDecription'));
// Vue.component('trade-description-agent', require('./components/TradeDescriptionAgent'));
Vue.component('trade-creation', require('./components/TradeCreation'));
Vue.use(BootstrapVue);
Vue.use(Vuelidate);
Vue.use(Vuex);

const store = new Vuex.Store({
    state:{
        trade:{
            user_role_name:"Buyer",
            seller_email:null,
            buyer_email:null,
            trade_name:null,
            description:null,
            trade_amount:null,
            delivery_time:null,
            inspection_period:null,
            isDelivery:false,
            isAgent:false,
            is_progress_payment:true,
            hasBeneficiary:false,
            isConfidential:false,
            hasDocuments:false,
            hasNotifications:false,
            escrow_fee:null,
            escrow_allocation:null,
            agent_email:null,
            agent_pay_type:null,
            agent_allocation:null,
            pre_agent_fee:null,
            agent_fee:null,
            buyer_fee:null,
            seller_fee:null,
            currency_used:'GHS',
            current_trade_category:null
        }
    },
    mutations: {
        changeUserRole(state, payload){
            state.trade.user_role_name = payload.role
        },
        addTradeParties(state, payload){
            state.trade.buyer_email = payload.buyer;
            state.trade.seller_email = payload.seller;
            state.trade.agent_email = payload.agent
        },
        addTradeDescription(state, payload){
            state.trade.trade_name = payload.name;
            state.trade.description = payload.description;
            state.trade.escrow_allocation = payload.admin_allocation;
            state.trade.current_trade_category = payload.category;
            state.trade.is_progress_payment = payload.isProgress;
            state.trade.delivery_time = payload.delivery_time;
            state.trade.inspection_period = payload.inspection_period;
            state.trade.trade_amount = payload.trade_amount;
            state.trade.buyer_fee = payload.buyer_fee;
            state.trade.seller_fee = payload.seller_fee;
        }
    }
});

const app = new Vue({
    el: '#create_trade',
    store,
    data() {
        return {
        }
    },
    validations:{}
});



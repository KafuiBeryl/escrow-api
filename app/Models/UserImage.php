<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 31 Jan 2019 19:24:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserImage
 * 
 * @property int $id
 * @property int $user_id
 * @property string $image_name
 * @property boolean $image_byte_array
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UserImage extends Eloquent
{
	protected $table = 'user_image';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'image_name',
		'image_byte_array'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}

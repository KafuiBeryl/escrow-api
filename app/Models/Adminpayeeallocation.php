<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Adminpayeeallocation
 * 
 * @property int $id
 * @property string $payee_allocation
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $trades
 *
 * @package App\Models
 */
class Adminpayeeallocation extends Eloquent
{
	protected $table = 'adminpayeeallocation';

    protected $casts = [
        'agent' => 'bool'
    ];

	protected $fillable = [
		'payee_allocation',
        'agent'
	];

    /*
    * Hide the listed fields (Attributes)
    * from the returned json object
    */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

	public function trades()
	{
		return $this->hasMany(\App\Models\Trade::class, 'escrow_fee_allocation');
	}
}

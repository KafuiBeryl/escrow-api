<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use App\Models\Adminpayeeallocation;
use App\Models\Country;
use App\Models\Deliveryaddress;
use App\Models\Document;
use App\Models\Feetype;
use App\Models\Notification;
use App\Models\Progresspayment;
use App\Models\Standardpayeeallocation;
use App\Models\Tradebeneficiary;
use App\Models\Tradestatus;
use App\Models\User;
use App\Models\Usertraderole;
use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Trade
 * 
 * @property int $id
 * @property int $user_id
 * @property int $user_role
 * @property string $seller_email
 * @property string $buyer_email
 * @property string $trade_name
 * @property string $description
 * @property int $trade_amount
 * @property string $delivery_time
 * @property string $inspection_period
 * @property bool $isDelivery
 * @property bool $is_progress_payment
 * @property bool $hasBeneficiary
 * @property bool $hasDocuments
 * @property bool $hasNotifications
 * @property int $escrow_fee
 * @property int $escrow_fee_allocation
 * @property string $agent_email
 * @property int $agent_fee_type
 * @property int $agent_fee_allocation
 * @property int $agent_fee
 * @property int $currency
 * @property int $trade_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Standardpayeeallocation $standardpayeeallocation
 * @property Feetype $feetype
 * @property Country $country
 * @property Adminpayeeallocation $adminpayeeallocation
 * @property Tradestatus $tradestatus
 * @property User $user
 * @property Usertraderole $usertraderole
 * @property \Illuminate\Database\Eloquent\Collection $deliveryaddresses
 * @property \Illuminate\Database\Eloquent\Collection $documents
 * @property \Illuminate\Database\Eloquent\Collection $notifications
 * @property \Illuminate\Database\Eloquent\Collection $progresspayments
 * @property \Illuminate\Database\Eloquent\Collection $tradebeneficiaries
 *
 * @package App\Models
 */
class Trade extends Eloquent
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $table = 'trade';

	protected $casts = [
		'user_id' => 'int',
		'user_role' => 'int',
		'trade_amount' => 'int',
		'isDelivery' => 'bool',
		'is_progress_payment' => 'bool',
		'hasBeneficiary' => 'bool',
        'isConfidential' => 'bool',
		'hasDocuments' => 'bool',
        'hasNotifications' => 'bool',
		'escrow_fee' => 'int',
		'escrow_fee_allocation' => 'int',
		'agent_fee_type' => 'int',
		'agent_fee_allocation' => 'int',
        'pre_agent_fee' => 'int',
		'agent_fee' => 'int',
		'currency' => 'int',
		'trade_status' => 'int'
	];

	protected $fillable = [
		'user_id',
		'user_role',
		'seller_email',
		'buyer_email',
		'trade_name',
		'description',
		'trade_amount',
		'delivery_time',
		'inspection_period',
		'isDelivery',
		'is_progress_payment',
		'hasBeneficiary',
        'isConfidential',
		'hasDocuments',
        'hasNotifications',
		'escrow_fee',
		'escrow_fee_allocation',
		'agent_email',
		'agent_fee_type',
		'agent_fee_allocation',
        'pre_agent_fee',
        'agent_fee',
		'currency',
		'trade_status',
        'tradeprocess_id',
        'trade_category'
	];

	public function standardPayeeAllocation()
	{
		return $this->belongsTo(Standardpayeeallocation::class, 'agent_fee_allocation');
	}

	public function feeType()
	{
		return $this->belongsTo(Feetype::class, 'agent_fee_type');
	}

	public function country()
	{
		return $this->belongsTo(Country::class, 'currency');
	}

	public function adminPayeeAllocation()
	{
		return $this->belongsTo(Adminpayeeallocation::class, 'escrow_fee_allocation');
	}

	public function tradeStatus()
	{
		return $this->belongsTo(Tradestatus::class, 'trade_status');
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function userTradeRole()
	{
		return $this->belongsTo(Usertraderole::class, 'user_role');
	}

	public function deliveryAddresses()
	{
		return $this->hasMany(Deliveryaddress::class);
	}

	public function documents()
	{
		return $this->hasMany(Document::class);
	}

	public function notifications()
	{
		return $this->hasMany(Notification::class);
	}

	public function progressPayments()
	{
		return $this->hasMany(Progresspayment::class);
	}

	public function tradeRoles(){
	    return $this->hasMany(TradeRole::class);
    }

	public function tradeBeneficiaries()
	{
		return $this->hasMany(Tradebeneficiary::class);
	}

	public function tradeStep()
    {
        return $this->belongsTo(Tradeprocess::class, 'tradeprocess_id');
    }

    public function tradeIndustry()
    {
	    return $this->belongsTo(Industry::class, 'trade_category');
    }
}

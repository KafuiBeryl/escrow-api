<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Industry
 * 
 * @property int $id
 * @property string $industry_classification
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Industry extends Eloquent
{
	protected $fillable = [
		'industry_classification'
	];

    public function trades()
    {
        return $this->hasMany(Trade::class, 'trade_category');
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use App\Models\Trade;
use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Progresspayment
 * 
 * @property int $id
 * @property int $trade_id
 * @property string $milestone_name
 * @property string $description
 * @property int $gross_amount
 * @property string $delivery_time
 * @property string $inspection_period
 * @property int $running_total
 * @property int $seller_fees
 * @property int $payable_to_seller
 * @property int $cumulative_payable
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Trade $trade
 *
 * @package App\Models
 */
class Progresspayment extends Eloquent
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $table = 'progresspayment';

	protected $casts = [
		'trade_id' => 'int',
		'gross_amount' => 'int',
		'running_total' => 'int',
		'seller_fees' => 'int',
		'payable_to_seller' => 'int',
		'cumulative_payable' => 'int'
	];

	protected $fillable = [
		'trade_id',
		'milestone_name',
		'description',
		'gross_amount',
		'delivery_time',
		'inspection_period',
		'running_total',
		'seller_fees',
		'payable_to_seller',
		'cumulative_payable'
	];

	public function trade()
	{
		return $this->belongsTo(Trade::class);
	}
}

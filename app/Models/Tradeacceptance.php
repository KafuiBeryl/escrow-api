<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 13 Feb 2019 15:49:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tradeacceptance
 * 
 * @property int $id
 * @property int $trade_id
 * @property bool $buyer_accept
 * @property bool $seller_accept
 * @property bool $agent_accept
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Trade $trade
 *
 * @package App\Models
 */
class Tradeacceptance extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'tradeacceptance';

	protected $casts = [
		'trade_id' => 'int',
		'buyer_accept' => 'bool',
		'seller_accept' => 'bool',
		'agent_accept' => 'bool'
	];

	protected $fillable = [
		'trade_id',
		'buyer_accept',
		'seller_accept',
		'agent_accept'
	];

	public function trade()
	{
		return $this->belongsTo(\App\Models\Trade::class);
	}
}

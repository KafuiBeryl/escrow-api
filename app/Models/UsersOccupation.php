<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Jan 2019 14:57:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsersOccupation
 * 
 * @property int $id
 * @property int $user_id
 * @property int $user_occupation
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\User $user
 * @property \App\Models\Occupation $occupation
 *
 * @package App\Models
 */
class UsersOccupation extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'users_occupation';

	protected $casts = [
		'user_id' => 'int',
		'user_occupation' => 'int'
	];

	protected $fillable = [
		'user_id',
		'user_occupation'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function occupation()
	{
		return $this->belongsTo(\App\Models\Occupation::class, 'user_occupation');
	}
}

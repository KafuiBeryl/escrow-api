<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Companycategory
 * 
 * @property int $id
 * @property string $category
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Companycategory extends Eloquent
{
	protected $fillable = [
		'category'
	];

    /*
   * Hide the listed fields (Attributes)
   * from the returned json object
   */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'company_type');
	}
}

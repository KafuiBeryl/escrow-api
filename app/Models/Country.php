<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use App\Models\Deliveryaddress;
use App\Models\Trade;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 * 
 * @property int $id
 * @property string $country_code
 * @property string $country_name
 * @property string $country_dialing_code
 * @property string $currency
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $deliveryaddresses
 * @property \Illuminate\Database\Eloquent\Collection $trades
 *
 * @package App\Models
 */
class Country extends Eloquent
{
    protected $casts = [
        'active' => 'bool'
    ];

	protected $fillable = [
		'country_code',
		'country_name',
		'country_dialing_code',
		'currency',
        'currency_name',
        'currency_code',
        'active'
	];

    /*
    * Hide the listed fields (Attributes)
    * from the returned json object
    */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

	public function deliveryaddresses()
	{
		return $this->hasMany(Deliveryaddress::class, 'country');
	}

	public function trades()
	{
		return $this->hasMany(Trade::class, 'currency');
	}
}

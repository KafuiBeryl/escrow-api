<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Document
 * 
 * @property int $id
 * @property int $trade_id
 * @property string $document_name
 * @property boolean $document
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Trade $trade
 *
 * @package App\Models
 */
class Document extends Eloquent
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $casts = [
		'trade_id' => 'int',
		'document' => 'boolean'
	];

	protected $fillable = [
		'trade_id',
		'document_name',
		'document'
	];

	public function trade()
	{
		return $this->belongsTo(\App\Models\Trade::class);
	}
}

<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 31 Oct 2018 15:50:39 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tradepayment
 * 
 * @property int $id
 * @property int $trade_id
 * @property int $amount_paid
 * @property int $user_id
 * @property bool $isSuccess
 * @property string $payment_mode
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Trade $trade
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Tradepayment extends Eloquent
{
	protected $casts = [
		'trade_id' => 'int',
		'amount_paid' => 'int',
		'user_id' => 'int',
		'isSuccess' => 'bool'
	];

	protected $fillable = [
		'trade_id',
		'amount_paid',
		'user_id',
		'isSuccess',
		'payment_mode',
        'status',
        'reason',
        'transaction_id'
	];

	public function trade()
	{
		return $this->belongsTo(\App\Models\Trade::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}

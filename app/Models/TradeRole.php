<?php

namespace App\Models;

use App\Models\Trade;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TradeRole
 *
 * @property int $id
 * @property int $trade_id
 * @property string $user_email
 * @property string $trade_name
 * @property int $user_role
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Usertraderole
 * @property Trade $trade
 *
 * @package App\Models
 */
class TradeRole extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'traderoles';

    protected $casts = [
        'trade_id' => 'int',
        'user_role' => 'int'
    ];

    protected $fillable = [
        'trade_id',
        'trade_name',
        'user_email',
        'user_role'
    ];

    public function trade()
    {
        return $this->belongsTo(Trade::class);
    }

    public function userTradeRole()
    {
        return $this->belongsTo(Usertraderole::class, 'user_role');
    }
}

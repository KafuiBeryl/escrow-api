<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Beneficiarytype
 * 
 * @property int $id
 * @property string $beneficiary_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $tradebeneficiaries
 *
 * @package App\Models
 */
class Beneficiarytype extends Eloquent
{
	protected $table = 'beneficiarytype';

	protected $fillable = [
		'beneficiary_type'
	];

	public function tradebeneficiaries()
	{
		return $this->hasMany(\App\Models\Tradebeneficiary::class, 'beneficiary_type');
	}
}

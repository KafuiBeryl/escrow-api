<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Feeschedule
 * 
 * @property int $id
 * @property int $from
 * @property int $to
 * @property string $percentage
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Feeschedule extends Eloquent
{
	protected $table = 'feeschedule';

	protected $casts = [
		'from' => 'int',
		'to' => 'int'
	];

	protected $fillable = [
		'from',
		'to',
		'percentage'
	];
}

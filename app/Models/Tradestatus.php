<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tradestatus
 * 
 * @property int $id
 * @property string $trade_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $trades
 *
 * @package App\Models
 */
class Tradestatus extends Eloquent
{
	protected $table = 'tradestatus';

	protected $fillable = [
		'trade_status'
	];

	public function trades()
	{
		return $this->hasMany(\App\Models\Trade::class, 'trade_status');
	}
}

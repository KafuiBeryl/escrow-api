<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Standardpayeeallocation
 * 
 * @property int $id
 * @property string $payee_allocation
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $trades
 * @property \Illuminate\Database\Eloquent\Collection $tradebeneficiaries
 *
 * @package App\Models
 */
class Standardpayeeallocation extends Eloquent
{
	protected $table = 'standardpayeeallocation';

	protected $fillable = [
		'payee_allocation'
	];

	public function trades()
	{
		return $this->hasMany(\App\Models\Trade::class, 'agent_fee_allocation');
	}

	public function tradebeneficiaries()
	{
		return $this->hasMany(\App\Models\Tradebeneficiary::class, 'beneficiary_payee');
	}
}

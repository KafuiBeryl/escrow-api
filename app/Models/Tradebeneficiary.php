<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use App\Models\Beneficiarytype;
use App\Models\Feetype;
use App\Models\Standardpayeeallocation;
use App\Models\Trade;
use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Tradebeneficiary
 * 
 * @property int $id
 * @property string $email
 * @property int $trade_id
 * @property int $beneficiary_type
 * @property int $beneficiary_fee_type
 * @property int $beneficiary_payee
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Feetype $feetype
 * @property Standardpayeeallocation $standardpayeeallocation
 * @property Beneficiarytype $beneficiarytype
 * @property Trade $trade
 *
 * @package App\Models
 */
class Tradebeneficiary extends Eloquent
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $table = 'tradebeneficiary';

	protected $casts = [
		'trade_id' => 'int',
		'beneficiary_type' => 'int',
		'beneficiary_fee_type' => 'int',
		'pre_beneficiary_fee' => 'int',
		'beneficiary_fee' => 'int',
		'beneficiary_payee' => 'int'
	];

	protected $fillable = [
		'email',
		'trade_id',
		'beneficiary_type',
		'beneficiary_fee_type',
        'pre_beneficiary_fee',
        'beneficiary_fee',
		'beneficiary_payee'
	];

	public function feeType()
	{
		return $this->belongsTo(Feetype::class, 'beneficiary_fee_type');
	}

	public function standardPayeeAllocation()
	{
		return $this->belongsTo(Standardpayeeallocation::class, 'beneficiary_payee');
	}

	public function beneficiaryType()
	{
		return $this->belongsTo(Beneficiarytype::class, 'beneficiary_type');
	}

	public function trade()
	{
		return $this->belongsTo(Trade::class);
	}
}

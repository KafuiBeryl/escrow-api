<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Identificationtype
 * 
 * @property int $id
 * @property string $identification_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Identificationtype extends Eloquent
{
	protected $table = 'identificationtype';

	protected $fillable = [
		'identification_type'
	];

    /*
    * Hide the listed fields (Attributes)
    * from the returned json object
    */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'identification_type');
	}
}

<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use App\Models\Trade;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Usertraderole
 * 
 * @property int $id
 * @property string $trade_role
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $trades
 *
 * @package App\Models
 */
class Usertraderole extends Eloquent
{
	protected $table = 'usertraderole';

	protected $fillable = [
		'trade_role'
	];

	public function trades()
	{
		return $this->hasMany(Trade::class, 'user_role');
	}

    public function tradeRoles()
    {
        return $this->hasMany(TradeRole::class, 'user_role');
    }
}

<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Bankaccounttype
 * 
 * @property int $id
 * @property string $account_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Bankaccounttype extends Eloquent
{
//    protected $table = 'bankaccounttypes';

	protected $fillable = [
		'account_type'
	];

    /*
   * Hide the listed fields (Attributes)
   * from the returned json object
   */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'account_type');
	}
}

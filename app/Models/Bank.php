<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Bank
 * 
 * @property int $id
 * @property string $bank_code
 * @property string $bank_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Bank extends Eloquent
{
//    protected $table = 'banks';

	protected $fillable = [
		'bank_code',
		'bank_name'
	];

    /*
   * Hide the listed fields (Attributes)
   * from the returned json object
   */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

	public function users()
	{
		return $this->hasMany(User::class);
	}
}

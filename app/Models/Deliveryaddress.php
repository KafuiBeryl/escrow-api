<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Deliveryaddress
 * 
 * @property int $id
 * @property int $trade_id
 * @property string $house_number
 * @property string $street_line_1
 * @property string $street_line_2
 * @property string $suburb
 * @property string $town_or_city
 * @property int $country
 * @property string $post_code
 * @property string $comments
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Trade $trade
 *
 * @package App\Models
 */
class Deliveryaddress extends Eloquent
{
    use SoftDeletes;
	protected $table = 'deliveryaddress';

    protected $dates = ['deleted_at'];

	protected $casts = [
		'trade_id' => 'int',
		'country_id' => 'int'
	];

	protected $fillable = [
		'trade_id',
		'house_number',
		'street_line_1',
		'street_line_2',
		'suburb',
		'town_or_city',
		'country_id',
		'digital_address',
		'comments'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class, 'country');
	}

	public function trade()
	{
		return $this->belongsTo(\App\Models\Trade::class);
	}
}

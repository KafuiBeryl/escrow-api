<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use App\Models\Bank;
use App\Models\Bankaccounttype;
use App\Models\Companycategory;
use App\Models\Identificationtype;
use App\Models\Trade;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Passwords\CanResetPassword;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $email_token
 * @property string $password
 * @property string $mobile_dialing_code
 * @property string $mobile_number
 * @property string $home_dialing_code
 * @property string $home_number
 * @property int $identification_type
 * @property string $id_number
 * @property string $citizenship
 * @property string $company_name
 * @property string $company_registration
 * @property int $company_type
 * @property string $bill_address_line_1
 * @property string $bill_address_line_2
 * @property string $bill_address_suburb
 * @property string $bill_address_city
 * @property string $bill_address_region
 * @property string $bill_address_country
 * @property string $bill_address_postcode
 * @property string $digital_address
 * @property int $bank_id
 * @property string $account_number
 * @property string $bank_branch
 * @property int $account_type
 * @property string $device_id
 * @property string $remember_token
 * @property string $user_account_completed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property Bankaccounttype $bankaccounttype
 * @property Bank $bank
 * @property Companycategory $companycategory
 * @property Identificationtype $identificationtype
 * @property \Illuminate\Database\Eloquent\Collection $trades
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes, CanResetPassword;

    protected $dates = ['deleted_at'];
	protected $casts = [
		'identification_type' => 'int',
		'company_type' => 'int',
		'bank_id' => 'int',
		'account_type' => 'int',
        'phone_verified' => 'bool',
        'email_confirmed' => 'bool',
        'user_account_completed' => 'bool'
	];

	protected $hidden = [
		'password',
		'remember_token',
        'email_token'
	];

	protected $fillable = [
		'first_name',
		'last_name',
		'email',
        'email_confirmed',
        'email_token',
		'password',
		'mobile_dialing_code',
		'mobile_number',
		'home_dialing_code',
		'home_number',
		'identification_type',
		'id_number',
		'citizenship',
		'company_name',
        'tax_identification_number',
		'company_registration',
		'company_type',
		'bill_address_line_1',
		'bill_address_line_2',
		'bill_address_suburb',
		'bill_address_city',
		'bill_address_region',
		'bill_address_country',
		'bill_address_postcode',
		'digital_address',
		'bank_id',
		'account_number',
        'account_name',
		'bank_branch',
		'account_type',
		'device_id',
        'phone_verified',
        'user_account_completed',
		'remember_token'
	];

	public function bankAccountType()
	{
		return $this->belongsTo(Bankaccounttype::class, 'account_type');
	}

	public function bank()
	{
		return $this->belongsTo(Bank::class);
	}

	public function companyCategory()
	{
		return $this->belongsTo(Companycategory::class, 'company_type');
	}

	public function identificationTypeUsed()
	{
		return $this->belongsTo(Identificationtype::class, 'identification_type');
	}

	public function trades()
	{
		return $this->hasMany(Trade::class);
	}

	public function image(){
	    return $this->hasOne(UserImage::class);
    }
}

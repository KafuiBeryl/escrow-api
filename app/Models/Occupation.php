<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 28 Jan 2019 14:57:07 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Occupation
 * 
 * @property int $id
 * @property int $rank
 * @property string $occupation
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Occupation extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'rank' => 'int'
	];

	protected $fillable = [
		'rank',
		'occupation'
	];

    /*
   * Hide the listed fields (Attributes)
   * from the returned json object
   */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'users_occupation', 'user_occupation')
					->withPivot('id', 'deleted_at')
					->withTimestamps();
	}
}

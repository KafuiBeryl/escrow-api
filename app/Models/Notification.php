<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 27 Apr 2018 15:10:15 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Notification
 * 
 * @property int $id
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property int $trade_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Trade $trade
 *
 * @package App\Models
 */
class Notification extends Eloquent
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
	protected $casts = [
		'trade_id' => 'int'
	];

	protected $fillable = [
		'email',
		'first_name',
		'last_name',
		'trade_id'
	];

	public function trade()
	{
		return $this->belongsTo(\App\Models\Trade::class);
	}
}

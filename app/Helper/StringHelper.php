<?php
/**
 * Created by PhpStorm.
 * User: NIT_APPDEV_002
 * Date: 5/4/2018
 * Time: 11:30
 */

namespace App\Helper;


class StringHelper
{
    public function isNullOrEmpty($question){
        return (!isset($question) || trim($question)==='' || strlen($question) == 0);
    }

    /**
     * retrieve buyer invitation string
     * @return string
     */
    public function buyerInviteString():string{
        $daString = "Hi, <br /> <br /> You have been invited to participate in a transaction as a buyer. <br />".
        "Please follow the link below to download the application and register for an account using this email address.".
            "If you already have an account please log in to get access to the trade you have been invited to. ".
        "We, the SafeTransact team look forward to facilitating a safe transaction for yourself and the other parties involved.";
        return $daString;
    }

    /**
     * retrieve seller invitation string
     * @return string
     */
    public function sellerInviteString():string{
        $daString = "Hi, <br /> <br /> You have been invited to participate in a transaction as a seller. <br />".
            "Please follow the link below to download the application and register for an account using this email address.".
            "If you already have an account please log in to get access to the trade you have been invited to. ".
            "We, the SafeTransact team look forward to facilitating a safe transaction for yourself and the other parties involved.";
        return $daString;
    }

    /**
     * retrieve agent invitation string
     * @return string
     */
    public function agentInviteString():string{
        $daString = "Hi, <br /> <br /> You have been invited to participate in a transaction as an agent. <br />".
            "Please follow the link below to download the application and register for an account using this email address.".
            "If you already have an account please log in to get access to the trade you have been invited to. ".
            "We, the SafeTransact team look forward to facilitating a safe transaction for yourself and the other parties involved.";
        return $daString;
    }

    /**
     * retrieve notification gang invitation string
     * @return string
     */
    public function notificationGangString():string{
        $daString = "<br /> <br /> You have been invited to participate in a transaction as a read-only observer. <br />".
            "Please follow the link below to download the application and register for an account using this email address.".
            "If you already have an account please log in to get access to the trade you have been invited to. ".
            "We, the SafeTransact team look forward to facilitating a safe transaction for yourself and the other parties involved.";
        return $daString;
    }

    /**
     * retrieve beneficiary invitation string
     * @return string
     */
    public function beneficiaryString():string{
        $daString = "Hi,<br /> <br /> You have been invited to participate in a transaction as a beneficiary. <br />".
            "Please follow the link below to download the application and register for an account using this email address.".
            "If you already have an account please log in to get access to the trade you have been invited to. ".
            "We, the SafeTransact team look forward to facilitating a safe transaction for yourself and the other parties involved.";
        return $daString;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: NIT_APPDEV_002
 * Date: 1/18/2019
 * Time: 12:56
 */

namespace App\Helper;

use Illuminate\Support\Facades\Log;
use Pusher\PushNotifications\PushNotifications;

class NotificationHelper
{
//    private $guzzle;
//
//    public function __construct()
//    {
//        $this->guzzle = new Client();
//    }

    /**
     * @param string $title
     * @param string $body
     * @param string $interest
     */
    public function sendNotification($interest, $title, $body){

        $pushNotifications = new PushNotifications([
            'instanceId' => (string) env('PUSHER_BEAMS_INSTANCE_ID'),
            'secretKey' => (string) env('PUSHER_BEAMS_TOKEN'),
        ]);

        try{
            $publishResponse = (array)$pushNotifications->publish(
                [$interest],
                [
                    'fcm'=> [
                        'notification' => [
                            'title' => $title,
                            'body' => $body,
                        ],
                        'mutable-content' => 0,
                        'category' => 'pusher',
                        'sound' => 'default',
                    ],
                ]
            );
            Log::info(implode($publishResponse));
        }catch (\Exception $exception){
            Log::warning($exception->getMessage());
        }
    }

    /**
     * @param string $title
     * @param string $body
     * @param string $interest
     */
//    public function sendNotification($interest, $body, $title){
//        $rawData = '{
//            "interests":["'.$interest.'"],
//            "fcm":{
//                "notification":{
//                    "title":"'.$title.'",
//                    "body":"'.$body.'"
//                }
//            }
//        }';
//
//        try{
//            $response = $this->guzzle->post('https://'.env('PUSHER_BEAMS_INSTANCE_ID').'.pushnotifications.pusher.com/publish_api/v1/instances/'.env('PUSHER_BEAMS_INSTANCE_ID').'/publishes', [
//                'headers' => [
//                    'authorization' => 'Bearer '.env('PUSHER_BEAMS_TOKEN'),
//                    'accept' => 'application/json',
//                    'content-type' => 'application/json; charset=UTF8',
//                    'timeout' => 5000,
//                ],
//                'body' => $rawData
//            ]);
//        }catch (RequestException $requestException){
//            Log::warning($requestException->getMessage());
//        }catch (\Exception $exception){
//            Log::warning($exception->getMessage());
//        }
//    }
}
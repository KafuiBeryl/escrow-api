<?php
/**
 * Created by PhpStorm.
 * User: NIT_APPDEV_002
 * Date: 6/19/2018
 * Time: 11:10
 */

namespace App\Helper;


use App\Models\Trade;
use App\Models\Tradeacceptance;
use App\Models\User;

class GeneralHelper
{
    /**
     * @param string $user_id
     */
    public function setAccountCompleted($user_id){
        $stringHelper = new StringHelper();
        $user = User::find($user_id);
        if ($stringHelper->isNullOrEmpty($user->first_name) &&
            $stringHelper->isNullOrEmpty($user->last_name) &&
            $user->email_confirmed == false && $stringHelper->isNullOrEmpty($user->id_number) &&
            $stringHelper->isNullOrEmpty($user->mobile_number) &&
            $stringHelper->isNullOrEmpty($user->account_number) &&
            $stringHelper->isNullOrEmpty($user->account_name) &&
            $stringHelper->isNullOrEmpty($user->bank_id) &&
            $stringHelper->isNullOrEmpty($user->acoount_type)){
            $user->user_account_completed = false;
            $user->save();
        }else{
            $user->user_account_completed = true;
            $user->save();
        }
    }

    /**
     * Format channnel identifier
     * @param integer $tradeId
     * @return string
     */
    public function createChannelIdentifier($tradeId){
        $channelString = $tradeId.'trade_negotiation_chat';
        return $channelString;
    }

    /**
     * @param string $role
     * @param integer $trade_id
     * @return void
     */
    public function createTradeAcceptance($role, $trade_id){
        if ($role == "Seller"){
            Tradeacceptance::create([
                'trade_id' => $trade_id,
                'buyer_accept' => false,
                'seller_accept' => true,
                'agent_accept' => false
            ]);
        }elseif ($role == "Buyer"){
            Tradeacceptance::create([
                'trade_id' => $trade_id,
                'buyer_accept' => true,
                'seller_accept' => false,
                'agent_accept' => false
            ]);
        }elseif ($role == "Agent"){
            Tradeacceptance::create([
                'trade_id' => $trade_id,
                'buyer_accept' => false,
                'seller_accept' => false,
                'agent_accept' => true
            ]);
        }
    }

    /**
     * @param string $role
     * @param integer $trade_id
     * @return void
     */
    public function addTradeAcceptance($role, $trade_id){
        $acceptance = Tradeacceptance::where('trade_id', $trade_id)->first();
        if ($role == "Seller"){
            $acceptance->seller_accept = true;
            $acceptance->save();
        }elseif ($role == "Buyer"){
            $acceptance->buyer_accept = true;
            $acceptance->save();
        }elseif ($role == "Agent"){
            $acceptance->agent_accept = true;
            $acceptance->save();
        }
    }

    /**
     * @param string $role
     * @param integer $trade_id
     * @return void
     */
    public function addTradeDeclined($role, $trade_id){
        $acceptance = Tradeacceptance::where('trade_id', $trade_id)->first();
        if ($role == "Seller"){
            $acceptance->seller_accept = false;
            $acceptance->save();
        }elseif ($role == "Buyer"){
            $acceptance->buyer_accept = false;
            $acceptance->save();
        }elseif ($role == "Agent"){
            $acceptance->agent_accept = false;
            $acceptance->save();
        }
    }

    /**
     * @param integer $trade_id
     * @return boolean
     */
    public function checkIfTradeIsAccepted($trade_id){
        $acceptance = Tradeacceptance::where('trade_id', $trade_id)->first();
        $trade = Trade::find($trade_id);
        if ($trade->agent_email == null){
            if ($acceptance->seller_accept = true && $acceptance->buyer_accept = true){
                return true;
            }else{
                return false;
            }
        }else{
            if ($acceptance->seller_accept = true && $acceptance->buyer_accept = true && $acceptance->agent_accept = true){
                return true;
            }else{
                return false;
            }
        }
    }
}
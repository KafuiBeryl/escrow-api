<?php
/**
 * Created by PhpStorm.
 * User: NIT_APPDEV_002
 * Date: 10/30/2018
 * Time: 15:22
 */

namespace App\Helper;


class PaymentHelper
{
    /**
     * Format amount to 12 digits as requested by API
     * @param integer $amount
     * @return integer
     */
    public function formatAmount($amount){
        $formattedAmount = str_pad($amount, 12, 0, STR_PAD_LEFT);
        return $formattedAmount;
    }

    /**
     * Format amount to 12 digits as requested by API
     * @param integer $tradeId
     * @return integer
     */
    public function createTransactionId($tradeId){
        $formattedId = str_pad($tradeId, 10, 0, STR_PAD_LEFT);
        //we only want a 2 digit number so we set the limit from 10 to just before hundred
        $randomDigits = rand(10, 99);
        $transactionId = str_pad($formattedId, 12, $randomDigits, STR_PAD_RIGHT);
        return $transactionId;
    }
}
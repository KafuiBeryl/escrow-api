<?php

namespace App\Http\Controllers;

use App\Models\Adminpayeeallocation;
use App\Models\Beneficiarytype;
use App\Models\Feeschedule;
use App\Models\Feetype;
use App\Models\Industry;
use App\Models\Standardpayeeallocation;
use Illuminate\Http\Request;
use NumberFormatter;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}

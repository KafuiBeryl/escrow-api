<?php

namespace App\Http\Controllers;

use App\Models\Adminpayeeallocation;
use App\Models\Beneficiarytype;
use App\Models\Feeschedule;
use App\Models\Feetype;
use App\Models\Industry;
use App\Models\Standardpayeeallocation;
use Illuminate\Http\Request;
use NumberFormatter;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Start the basic trade creation process
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feeSchedule = Feeschedule::all();
        $feeCollection = collect();
        $feeSchedule->each(function ($item) use(&$feeCollection){
            $formatter = new NumberFormatter('en_GH',  NumberFormatter::CURRENCY);
            $editItem['from'] = $formatter->formatCurrency(($item['from']/100), 'GHS');
            $editItem['to'] = $formatter->formatCurrency(($item['to']/100), 'GHS');
            $editItem['percentage'] = $item['percentage']."%";
            $feeCollection->push($editItem);
        });
//        print_r($feeArray);
//        die();
        $feeType = Feetype::all();
        $standardFeeAllocation = Standardpayeeallocation::all();
        $adminFeeAllocationAgent = Adminpayeeallocation::all();
        $adminFeeAllocation = Adminpayeeallocation::where('agent', false)->get();
        $beneficiaryType = Beneficiarytype::all();
        $industry = Industry::all();
        return view('pages.trade.create.trade_start')->with([
            'feeSchedule' => $feeCollection,
            'feeType' => $feeType,
            'standardFeeAllocation' => $standardFeeAllocation,
            'adminFeeAllocationAgent' => $adminFeeAllocationAgent,
            'adminFeeAllocation' => $adminFeeAllocation,
            'beneficiaryType' => $beneficiaryType,
            'industry' => $industry
        ]);
    }
}

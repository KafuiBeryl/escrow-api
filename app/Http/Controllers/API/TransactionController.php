<?php

namespace App\Http\Controllers\API;

use App\Helper\GeneralHelper;
use App\Helper\StringHelper;
use App\Models\Adminpayeeallocation;
use App\Models\Beneficiarytype;
use App\Models\Country;
use App\Models\Deliveryaddress;
use App\Models\Feetype;
use App\Models\Industry;
use App\Models\Notification;
use App\Models\Progresspayment;
use App\Models\Standardpayeeallocation;
use App\Models\Trade;
use App\Models\Tradebeneficiary;
use App\Models\TradeRole;
use App\Models\Usertraderole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    private $successStatus = 200;

    private $errorStatus = 400;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    //start transaction
    /**
     * start trade api endpoint
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function startBasicTrade(Request $request){
        $validator = Validator::make($request->all(), [
            'user_role_name' => 'bail|sometimes|required|exists:usertraderole,trade_role',
            'seller_email' => 'bail|sometimes|required|email|max:120',
            'buyer_email' => 'bail|sometimes|required|email|max:120',
            'trade_name' => 'bail|sometimes|required|max:100',
            'description' => 'bail|required|sometimes|max:5000',
            'trade_amount' => 'bail|sometimes|required_unless:is_progress_payment,true|integer',
            'delivery_time' => 'bail|sometimes|required_unless:is_progress_payment,true|max:10',
            'inspection_period' => 'bail|sometimes|required_unless:is_progress_payment,true|max:10',
            'isDelivery' => 'required|sometimes|boolean',
            'is_progress_payment' => 'bail|sometimes|required|boolean',
            'hasBeneficiary' => 'bail|sometimes|required|boolean',
            'isConfidential' => 'bail|sometimes|required|boolean',
            'hasDocuments' => 'bail|sometimes|required|boolean',
            'hasNotifications' => 'bail|sometimes|required|boolean',
            'escrow_fee' => 'bail|sometimes|required|integer',
            'escrow_allocation' => 'bail|sometimes|required|exists:adminpayeeallocation,payee_allocation',
            'agent_email' => 'bail|sometimes|nullable|email|max:120',
            'agent_pay_type' => 'bail|sometimes|nullable|exists:feetype,fee_type',
            'agent_allocation' => 'bail|sometimes|nullable|exists:standardpayeeallocation,payee_allocation',
            'pre_agent_fee' => 'bail|sometimes|required|integer',
            'agent_fee' => 'bail|sometimes|required|integer',
            'buyer_fee' => 'bail|sometimes|required|integer',
            'seller_fee' => 'bail|sometimes|required|integer',
            'currency_used' => 'bail|sometimes|required|exists:countries,currency_code',
            'current_trade_category' => 'bail|sometimes|required|exists:industries,industry_classification'
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('user_role_name'));
            array_push($array,$errors->first('seller_email'));
            array_push($array,$errors->first('buyer_email'));
            array_push($array,$errors->first('trade_name'));
            array_push($array,$errors->first('description'));
            array_push($array,$errors->first('trade_amount'));
            array_push($array,$errors->first('delivery_time'));
            array_push($array,$errors->first('inspection_period'));
            array_push($array,$errors->first('isDelivery'));
            array_push($array,$errors->first('is_progress_payment'));
            array_push($array,$errors->first('hasBeneficiary'));
            array_push($array,$errors->first('isConfidential'));
            array_push($array,$errors->first('hasDocuments'));
            array_push($array,$errors->first('hasNotifications'));
            array_push($array,$errors->first('escrow_fee'));
            array_push($array,$errors->first('escrow_allocation'));
            array_push($array,$errors->first('agent_email'));
            array_push($array,$errors->first('agent_pay_type'));
            array_push($array,$errors->first('agent_allocation'));
            array_push($array,$errors->first('pre_agent_fee'));
            array_push($array,$errors->first('agent_fee'));
            array_push($array,$errors->first('buyer_fee'));
            array_push($array,$errors->first('seller_fee'));
            array_push($array,$errors->first('currency_used'));
            array_push($array,$errors->first('current_trade_category'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $user = Auth::user();
            $input = $request->all();
            $input['user_id'] = $user->id;
            //set trade status to ongoing
            $input['trade_status'] = 3;
            //set trade process id to "Invitation to be sent"
            $input['tradeprocess_id'] = 1;
            $trade = Trade::create($input);

//            set trade acceptanceof trade creator to true
            $generalHelper = new GeneralHelper();
            $generalHelper->createTradeAcceptance($input['user_role_name'], $trade->id);

            //check if user trade role exists in the array before associating
            if (array_key_exists('user_role_name', $input)){
                $tradeRole = Usertraderole::where('trade_role', $input['user_role_name'])->first();
                $trade->userTradeRole()->associate($tradeRole);
            }

            if (array_key_exists('escrow_allocation', $input)){
                $escrowAllocation = Adminpayeeallocation::where('payee_allocation', $input['escrow_allocation'])->first();
                $trade->adminPayeeAllocation()->associate($escrowAllocation);
            }

            if (array_key_exists('agent_allocation', $input)){
                $agentAllocation = Standardpayeeallocation::where('payee_allocation', $input['agent_allocation'])->first();
                $trade->standardPayeeAllocation()->associate($agentAllocation);
            }

            if (array_key_exists('agent_pay_type', $input)){
                $agentFeeType = Feetype::where('fee_type', $input['agent_pay_type'])->first();
                $trade->feeType()->associate($agentFeeType);
            }

            if (array_key_exists('currency_used', $input)){
                $currency = Country::where('currency_code', $input['currency_used'])->first();
                $trade->country()->associate($currency);
            }

            if (array_key_exists('current_trade_category', $input)){
                $tradeCategory = Industry::where('industry_classification', $input['current_trade_category'])->first();
                $trade->tradeIndustry()->associate($tradeCategory);
            }

            $trade->save();

            //store the user and their trade role in the usertraderole table
            $tradeRoleCreated['trade_id'] = $trade->id;
            $tradeRoleCreated['trade_name'] = $trade->trade_name;
            $tradeRoleCreated['user_email'] = $user->email;
            $tradeRoleCreated['user_role'] = $trade->id;
            $createdRole = TradeRole::create($tradeRoleCreated);
            $role = Usertraderole::where('trade_role', $input['user_role_name'])->first();
            $createdRole->userTradeRole()->associate($role);

            $createdRole->save();

            //create JSON holder for the response
            $success['trade_id'] = $trade->id;

            $userTrades = Trade::where('user_id', $user->id)->with([
                'standardPayeeAllocation',
                'feeType',
                'country',
                'adminPayeeAllocation',
                'tradeStatus',
                'userTradeRole',
                'tradeStep',
                'tradeIndustry'
            ])->get();
            $success['user_trades'] = $userTrades;

            $tradeRole = TradeRole::where('user_email', $user->email)->with(
                'userTradeRole'
            )->get();
            $success['user_trade_roles'] = $tradeRole;

            return response()->json($success, $this-> successStatus);
        }
    }

    //add trade extras
    /**
     * add trade extras api endpoint
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function addTradeExtras(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id',
            'milestones' => 'bail|nullable|array',
            'beneficiaries' => 'bail|nullable|array',
            'notificationGang' => 'bail|nullable|array',
            'deliveryAddress' => 'bail|nullable'
        ]);
        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            array_push($array,$errors->first('milestones'));
            array_push($array,$errors->first('beneficiaries'));
            array_push($array,$errors->first('notificationGang'));
            array_push($array,$errors->first('deliveryAddress'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $input = $request->all();
            $trade = Trade::find($input['trade_id']);
            if (!empty(array_filter($input['milestones']))){
                foreach ($input['milestones'] as $milestone){
                    $milestone['trade_id'] = $input['trade_id'];

                    Progresspayment::create($milestone);
                }
            }

            if (!empty(array_filter($input['beneficiaries']))){
                foreach ($input['beneficiaries'] as $beneficiary){
                    $beneficiary['trade_id'] = $input['trade_id'];

                    $beneficiaryFeeAllocation = Standardpayeeallocation::where('payee_allocation', $beneficiary['beneficiary_payee'])->first();
                    $beneficiary['beneficiary_payee'] = $beneficiaryFeeAllocation->id;

                    $beneficiaryFeeType = Feetype::where('fee_type', $beneficiary['beneficiary_fee_type'])->first();
                    $beneficiary['beneficiary_fee_type'] = $beneficiaryFeeType->id;

                    $beneficiaryType = Beneficiarytype::where('beneficiary_type', $beneficiary['beneficiary_type'])->first();
                    $beneficiary['beneficiary_type'] = $beneficiaryType->id;

                    Tradebeneficiary::create($beneficiary);

                    //add beneficiary to the usertraderole table
                    // already done in invitation
//                    TradeRole::create([
//                        'trade_id' => $trade->id,
//                        'trade_name' => $trade->name,
//                        'user_email' => $beneficiary['email'],
//                        'user_role' => 5
//                    ]);
                }
            }

            if (!empty(array_filter($input['notificationGang']))){
                foreach ($input['notificationGang'] as $notificationGang){
                    $notificationGang['trade_id'] = $input['trade_id'];

                    Notification::create($notificationGang);

                    //add beneficiary to the usertraderole table
                    // already done in invitation
//                    TradeRole::create([
//                        'trade_id' => $trade->id,
//                        'trade_name' => $trade->name,
//                        'user_email' => $notificationGang['email'],
//                        'user_role' => 4
//                    ]);
                }
            }

            if (!empty($input['deliveryAddress'])){
//                foreach ($input['deliveryAddress'] as $deliveryAddress){
//
//                }
                $deliveryAddress = $input['deliveryAddress'];
                $deliveryAddress['trade_id'] = $input['trade_id'];

                $country = Country::where('country_name', $deliveryAddress['country_name'])->first();
                $deliveryAddress['country_id'] = $country->id;

                Deliveryaddress::create($deliveryAddress);
            }

            $milestone = Progresspayment::where('trade_id', $input['trade_id'])->get();
            if ($milestone->count() > 0){
                $success['milestones'] = $milestone;
            }

            $beneficiary = Tradebeneficiary::where('trade_id', $input['trade_id'])-> with([
                'standardPayeeAllocation',
                'feeType',
                'beneficiaryType'
            ])->get();
            if ($beneficiary->count() > 0){
                $success['beneficiaries'] = $beneficiary;
            }

            $notifications = Notification::where('trade_id', $input['trade_id'])->get();
            if ($notifications->count() > 0){
                $success['notifications'] = $notifications;
            }

            $delivery = Deliveryaddress::where('trade_id', $input['trade_id'])->get();
            if ($delivery->count() > 0){
                $success['delivery'] = $delivery;
            }

            return response()->json($success, $this-> successStatus);
        }
    }

    //get trade extras if any exist
    /**
     * get trade extras if any exist
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getTradeExtras(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id'
        ]);
        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $input = $request->all();

            $milestone = Progresspayment::where('trade_id', $input['trade_id'])->get();
            if ($milestone->count() > 0){
                $success['milestones'] = $milestone;
            }

            $beneficiary = Tradebeneficiary::where('trade_id', $input['trade_id'])-> with([
                'standardPayeeAllocation',
                'feeType',
                'beneficiaryType'
            ])->get();
            if ($beneficiary->count() > 0){
                $success['beneficiaries'] = $beneficiary;
            }

            $notifications = Notification::where('trade_id', $input['trade_id'])->get();
            if ($notifications->count() > 0){
                $success['notifications'] = $notifications;
            }

            $delivery = Deliveryaddress::where('trade_id', $input['trade_id'])->get();
            if ($delivery->count() > 0){
                $success['delivery'] = $delivery;
            }

            return response()->json($success, $this-> successStatus);
        }
    }

    //get all transactions handles by the user
//    /**
//     * get all trades initiated by user
//     * @return \Illuminate\Http\Response
//     */
//    public function getAllInitiatedTrades(){
//        $user = Auth::user();
//        $userTrades = Trade::where('user_id', $user->id)->with([
//            'standardPayeeAllocation',
//            'feeType',
//            'country',
//            'adminPayeeAllocation',
//            'tradeStatus',
//            'userTradeRole',
//            'tradeStep',
//            'tradeIndustry'
//        ])->get();
//        $success['user_trades'] = $userTrades;
//        return response()->json($success, $this->successStatus);
//    }

    //get transaction by id
    /**
     * get trade by id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function getTradeById(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id'
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $input = $request->all();
            $userTrades = Trade::find($input['trade_id'])->with([
                'standardPayeeAllocation',
                'feeType',
                'country',
                'adminPayeeAllocation',
                'tradeStatus',
                'userTradeRole',
                'tradeStep',
                'tradeIndustry'
            ])->first();
            $success['trade'] = $userTrades;
            return response()->json($success, $this->successStatus);
        }
    }

    //get all transactions the user is involved in
    /**
     * get transactions the user is involved in
     * @return \Illuminate\Http\Response
     */
    public function getInvolvedTrades(){
        $user = Auth::user();
        $email = $user->email;
        $userTrades = Trade::where('buyer_email', $email)
            ->orWhere('seller_email', $email)
            ->orWhere('agent_email', $email)->with([
                'standardPayeeAllocation',
                'feeType',
                'country',
                'adminPayeeAllocation',
                'tradeStatus',
                'userTradeRole',
                'tradeStep',
                'tradeIndustry'
            ])->get();

        $tradeRole = TradeRole::where('user_email', $user->email)->with(
            'userTradeRole'
        )->get();

        if ($tradeRole->count() > 0){
            $success['user_trade_roles'] = $tradeRole;
        }
        $success['user_invited_trades'] = $userTrades;
        return response()->json($success, $this->successStatus);
    }

    //edit trade
    /**
     * Edit trade
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function editTransaction(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id',
            'seller_email' => 'bail|sometimes|required|email|max:120',
            'buyer_email' => 'bail|sometimes|required|email|max:120',
            'trade_name' => 'bail|sometimes|required|max:100',
            'description' => 'bail|required|sometimes|max:5000',
            'trade_amount' => 'bail|sometimes|required_unless:is_progress_payment,true|integer',
            'delivery_time' => 'bail|sometimes|required_unless:is_progress_payment,true|max:10',
            'inspection_period' => 'bail|sometimes|required_unless:is_progress_payment,true|max:10',
            'isDelivery' => 'required|sometimes|boolean',
            'is_progress_payment' => 'bail|sometimes|required|boolean',
            'hasBeneficiary' => 'bail|sometimes|required|boolean',
            'isConfidential' => 'bail|sometimes|required|boolean',
            'hasDocuments' => 'bail|sometimes|required|boolean',
            'hasNotifications' => 'bail|sometimes|required|boolean',
            'escrow_fee' => 'bail|sometimes|required|integer',
            'escrow_allocation' => 'bail|sometimes|required|exists:adminpayeeallocation,payee_allocation',
            'agent_email' => 'bail|sometimes|nullable|email|max:120',
            'agent_pay_type' => 'bail|sometimes|nullable|exists:feetype,fee_type',
            'agent_allocation' => 'bail|sometimes|nullable|exists:standardpayeeallocation,payee_allocation',
            'pre_agent_fee' => 'bail|sometimes|required|integer',
            'agent_fee' => 'bail|sometimes|required|integer',
            'buyer_fee' => 'bail|sometimes|required|integer',
            'seller_fee' => 'bail|sometimes|required|integer',
            'currency_used' => 'bail|sometimes|required|exists:countries,currency_code',
            'current_trade_category' => 'bail|sometimes|required|exists:industries,industry_classification'
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            array_push($array,$errors->first('seller_email'));
            array_push($array,$errors->first('buyer_email'));
            array_push($array,$errors->first('trade_name'));
            array_push($array,$errors->first('description'));
            array_push($array,$errors->first('trade_amount'));
            array_push($array,$errors->first('delivery_time'));
            array_push($array,$errors->first('inspection_period'));
            array_push($array,$errors->first('isDelivery'));
            array_push($array,$errors->first('is_progress_payment'));
            array_push($array,$errors->first('hasBeneficiary'));
            array_push($array,$errors->first('isConfidential'));
            array_push($array,$errors->first('hasDocuments'));
            array_push($array,$errors->first('hasNotifications'));
            array_push($array,$errors->first('escrow_fee'));
            array_push($array,$errors->first('escrow_allocation'));
            array_push($array,$errors->first('agent_email'));
            array_push($array,$errors->first('agent_pay_type'));
            array_push($array,$errors->first('agent_allocation'));
            array_push($array,$errors->first('pre_agent_fee'));
            array_push($array,$errors->first('agent_fee'));
            array_push($array,$errors->first('buyer_fee'));
            array_push($array,$errors->first('seller_fee'));
            array_push($array,$errors->first('currency_used'));
            array_push($array,$errors->first('current_trade_category'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
//            $user = Auth::user();
            $input = $request->all();
//            foreach ($input as $key => $value){
//
//            }
            Trade::where('id', $input['trade_id'])->update($input);
            $trade = Trade::find($input['trade_id']);
            if (array_key_exists('escrow_allocation', $input)){
                $escrowAllocation = Adminpayeeallocation::where('payee_allocation', $input['escrow_allocation'])->first();
                $trade->adminPayeeAllocation()->associate($escrowAllocation);
            }

            if (array_key_exists('agent_allocation', $input)){
                $agentAllocation = Standardpayeeallocation::where('payee_allocation', $input['agent_allocation'])->first();
                $trade->standardPayeeAllocation()->associate($agentAllocation);
            }

            if (array_key_exists('agent_pay_type', $input)){
                $agentFeeType = Feetype::where('fee_type', $input['agent_pay_type'])->first();
                $trade->feeType()->associate($agentFeeType);
            }

            if (array_key_exists('currency_used', $input)){
                $currency = Country::where('currency_code', $input['currency_used'])->first();
                $trade->country()->associate($currency);
            }

            if (array_key_exists('current_trade_category', $input)){
                $tradeCategory = Industry::where('industry_classification', $input['current_trade_category'])->first();
                $trade->tradeIndustry()->associate($tradeCategory);
            }
        }

        $success['trade'] = $trade->with([
            'standardPayeeAllocation',
            'feeType',
            'country',
            'adminPayeeAllocation',
            'tradeStatus',
            'userTradeRole',
            'tradeStep',
            'tradeIndustry'
        ])->get();

        return response()->json($success, $this-> successStatus);
    }

    //edit trade extras
    /**
     * edit trade extras api endpoint
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function editTradeExtras(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id',
            'milestones' => 'bail|nullable|array',
            'beneficiaries' => 'bail|nullable|array',
            'notificationGang' => 'bail|nullable|array',
            'deliveryAddress' => 'bail|nullable'
        ]);
        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            array_push($array,$errors->first('milestones'));
            array_push($array,$errors->first('beneficiaries'));
            array_push($array,$errors->first('notificationGang'));
            array_push($array,$errors->first('deliveryAddress'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $input = $request->all();
            $trade = Trade::find($input['trade_id']);
            Progresspayment::where('trade_id', $input['trade_id'])->delete();
            Tradebeneficiary::where('trade_id', $input['trade_id'])->delete();
            Notification::where('trade_id', $input['trade_id'])->delete();
            Deliveryaddress::where('trade_id', $input['trade_id'])->delete();
//            //delete beneficiary roles for trade in question
//            TradeRole::where('trade_id', $trade->id)
//                ->where('user_role', 4)
//                ->delete();
//            //delete notification roles for trade in question
//            TradeRole::where('trade_id', $trade->id)
//                ->where('user_role', 5)
//                ->delete();

            if (!empty(array_filter($input['milestones']))){
                foreach ($input['milestones'] as $milestone){
                    $milestone['trade_id'] = $input['trade_id'];

                    Progresspayment::create($milestone);
                }
            }

            if (!empty(array_filter($input['beneficiaries']))){

                foreach ($input['beneficiaries'] as $beneficiary){
                    $beneficiary['trade_id'] = $input['trade_id'];

                    $beneficiaryFeeAllocation = Standardpayeeallocation::where('payee_allocation', $beneficiary['beneficiary_payee'])->first();
                    $beneficiary['beneficiary_payee'] = $beneficiaryFeeAllocation->id;

                    $beneficiaryFeeType = Feetype::where('fee_type', $beneficiary['beneficiary_fee_type'])->first();
                    $beneficiary['beneficiary_fee_type'] = $beneficiaryFeeType->id;

                    $beneficiaryType = Beneficiarytype::where('beneficiary_type', $beneficiary['beneficiary_type'])->first();
                    $beneficiary['beneficiary_type'] = $beneficiaryType->id;

                    Tradebeneficiary::create($beneficiary);

                    //add beneficiary to the usertraderole table
                    // already done in invitation
//                    TradeRole::create([
//                        'trade_id' => $trade->id,
//                        'trade_name' => $trade->name,
//                        'user_email' => $beneficiary['email'],
//                        'user_role' => 5
//                    ]);
                }
            }

            if (!empty(array_filter($input['notificationGang']))){

                foreach ($input['notificationGang'] as $notificationGang){
                    $notificationGang['trade_id'] = $input['trade_id'];

                    Notification::create($notificationGang);

                    //add beneficiary to the usertraderole table
                    // already done in invitation
//                    TradeRole::create([
//                        'trade_id' => $trade->id,
//                        'trade_name' => $trade->name,
//                        'user_email' => $notificationGang['email'],
//                        'user_role' => 4
//                    ]);
                }
            }

            if (!empty($input['deliveryAddress'])){
                $deliveryAddress = $input['deliveryAddress'];
                $deliveryAddress['trade_id'] = $input['trade_id'];

                $country = Country::where('country_name', $deliveryAddress['country_name'])->first();
                $deliveryAddress['country_id'] = $country->id;

                Deliveryaddress::create($deliveryAddress);
            }

            $milestone = Progresspayment::where('trade_id', $input['trade_id'])->get();
            if ($milestone->count() > 0){
                $success['milestones'] = $milestone;
            }

            $beneficiary = Tradebeneficiary::where('trade_id', $input['trade_id'])-> with([
                'standardPayeeAllocation',
                'feeType',
                'beneficiaryType'
            ])->get();
            if ($beneficiary->count() > 0){
                $success['beneficiaries'] = $beneficiary;
            }

            $notifications = Notification::where('trade_id', $input['trade_id'])->get();
            if ($notifications->count() > 0){
                $success['notifications'] = $notifications;
            }

            $delivery = Deliveryaddress::where('trade_id', $input['trade_id'])->get();
            if ($delivery->count() > 0){
                $success['delivery'] = $delivery;
            }

            return response()->json($success, $this-> successStatus);
        }
    }

}
<?php

namespace App\Http\Controllers\API;

use App\Helper\GeneralHelper;
use App\Helper\NotificationHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Pusher\Pusher;
use Pusher\PusherException;

class ChatController extends Controller
{
    private $successStatus = 200;

    private $errorStatus = 400;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function sendMessage(Request $request){
        $validator = Validator::make($request->all(), [
            'user' => 'bail|required|string',
            'message' => 'bail|nullable|sometimes|string',
            'time' => 'bail|sometimes|required',
            'image' => 'bail|sometimes|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:140',
            'trade_id' => 'bail|required|exists:trade,id'
        ]);
        if ($validator->fails()){
//            $error['error'] = 'please enter a valid message';
            return response()->json($this->errorStatus);
        }else{
            try{
                $input = $request->all();
                $options = array(
                    'cluster' => env('PUSHER_APP_CLUSTER'),
                    'useTLS' => true
                );

                $pusher = new Pusher(
                    env('PUSHER_APP_KEY'),
                    env('PUSHER_APP_SECRET'),
                    env('PUSHER_APP_ID'),
                    $options
                );
                $data['user'] = $input['user'];
                $data['message'] = $input['message'];
                $data['time'] = $input['time'];

                if ($request->hasFile('image')){
                    $file = $request->file('image');
                    $request->file('image')->store('public/chat/images');
                    $data['image'] = asset(Storage::url('chat/images/'.$file->hashName()));
                }else{
                    $data['image'] = null;
                }

                $generalHelper = new GeneralHelper();
                $channelIdentifier = $generalHelper->createChannelIdentifier($input['trade_id']);
                $pusher->trigger($channelIdentifier, "new_message", $data);
                return response()->json($this-> successStatus);
            }catch (PusherException $pusherException){
                return response()->json($this->errorStatus);
            }
        }
    }
}

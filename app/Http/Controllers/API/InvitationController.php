<?php

namespace App\Http\Controllers\API;

use App\Helper\StringHelper;
use App\Mail\SendTradeInvitation;
use App\Models\Notification;
use App\Models\Trade;
use App\Models\Tradebeneficiary;
use App\Models\TradeRole;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class InvitationController extends Controller
{
    public $successStatus = 200;

    public $errorStatus = 400;

    private $stringHelper;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->stringHelper = new StringHelper();
    }



    //send invitations to other parties of the trade
    /**
     * start trade api endpoint
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendInvitation(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id'
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $input = $request->all();
            $trade = Trade::find($input['trade_id']);
            $array=[];
            if ($trade->user_role === "Buyer"){
                $sellerMessage = $this->stringHelper->sellerInviteString();
                try{
                    $tradeRole = new TradeRole();
                    $tradeRole->trade_id = $trade->id;
                    $tradeRole->user_email = $trade->seller_email;
                    $tradeRole->user_role = 2;
                    $tradeRole->save();
                    $emailBody = new SendTradeInvitation($sellerMessage);
                    Mail::to($trade->seller_email)->send($emailBody);
                }catch (Exception $exception){
                    array_push($array,"seller email was not sent");
                }

                if (!($this->stringHelper->isNullOrEmpty($trade->agent_email))){
                    $agentMessage =  $this->stringHelper->agentInviteString();
                    try{
                        $tradeRole = new TradeRole();
                        $tradeRole->trade_id = $trade->id;
                        $tradeRole->user_email = $trade->agent_email;
                        $tradeRole->user_role = 3;
                        $tradeRole->save();
                        $email = new SendTradeInvitation($agentMessage);
                        Mail::to($trade->agent_email)->send($email);
                    }catch (Exception $exception){
                        array_push($array,"agent email was not sent");
                    }
                }
            }elseif ($trade->user_role === "Seller"){
                $buyerMessage = $this->stringHelper->buyerInviteString();
                try{
                    $tradeRole = new TradeRole();
                    $tradeRole->trade_id = $trade->id;
                    $tradeRole->user_email = $trade->buyer_email;
                    $tradeRole->user_role = 1;
                    $tradeRole->save();
                    $emailBody = new SendTradeInvitation($buyerMessage);
                    Mail::to($trade->buyer_email)->send($emailBody);
                }catch (Exception $exception){
                    array_push($array,"buyer email was not sent");
                }

                if (!($this->stringHelper->isNullOrEmpty($trade->agent_email))){
                    $agentMessage =  $this->stringHelper->agentInviteString();
                    try{
                        $tradeRole = new TradeRole();
                        $tradeRole->trade_id = $trade->id;
                        $tradeRole->user_email = $trade->agent_email;
                        $tradeRole->user_role = 3;
                        $tradeRole->save();
                        $email = new SendTradeInvitation($agentMessage);
                        Mail::to($trade->agent_email)->send($email);
                    }catch (Exception $exception){
                        array_push($array,"agent email was not sent");
                    }
                }
            }elseif ($trade->user_role === "Agent"){
                $buyMessage = $this->stringHelper->buyerInviteString();
                try{
                    $tradeRole = new TradeRole();
                    $tradeRole->trade_id = $trade->id;
                    $tradeRole->user_email = $trade->buyer_email;
                    $tradeRole->user_role = 1;
                    $tradeRole->save();
                    $emailBody = new SendTradeInvitation($buyMessage);
                    Mail::to($trade->buyer_email)->send($emailBody);
                }catch (Exception $exception){
                    array_push($array,"buyer email was not sent");
                }
                $sellMessage = $this->stringHelper->sellerInviteString();
                try{
                    $tradeRole = new TradeRole();
                    $tradeRole->trade_id = $trade->id;
                    $tradeRole->user_email = $trade->seller_email;
                    $tradeRole->user_role = 2;
                    $tradeRole->save();
                    $email = new SendTradeInvitation($sellMessage);
                    Mail::to($trade->seller_email)->send($email);
                }catch (Exception $exception){
                    array_push($array,"seller email was not sent");
                }
            }

            foreach (Notification::where('trade_id',  $trade->id)->cursor() as $notificationGang){
                $message = "Hi ".$notificationGang->first_name." ".$notificationGang->last_name.
                    $this->stringHelper->notificationGangString();
                try{
                    $tradeRole = new TradeRole();
                    $tradeRole->trade_id =  $trade->id;
                    $tradeRole->user_email = $notificationGang->email;
                    $tradeRole->user_role = 4;
                    $tradeRole->save();
                    $email = new SendTradeInvitation($message);
                    Mail::to($notificationGang->email)->send($email);
                }catch (Exception $exception){
                    array_push($array,"some notification emails were not sent successfully");
                }
            }

            foreach (Tradebeneficiary::where('trade_id', $trade->id)->cursor() as $beneficiary){
                try {
                    $tradeRole = new TradeRole();
                    $tradeRole->trade_id = $trade->id;
                    $tradeRole->user_email = $beneficiary->email;
                    $tradeRole->user_role = 5;
                    $tradeRole->save();
                    $email = new SendTradeInvitation($this->stringHelper->beneficiaryString());
                    Mail::to($beneficiary->email)->send($email);
                }catch (Exception $exception){
                    array_push($array,"some beneficiary emails were not sent successfully");
                }
            }
            $trade->tradeprocess_id = 2;
            $trade->save();

            $userTrades = $trade->with([
                'standardPayeeAllocation',
                'feeType',
                'country',
                'adminPayeeAllocation',
                'tradeStatus',
                'userTradeRole',
                'tradeStep',
                'tradeIndustry'
            ])->first();
            $success['trade'] = $userTrades;
            $success['unfortunately'] = implode(array_filter($array));
            $success['success'] = "Emails sending has been initiated";
            return response()->json($success, $this-> successStatus);
        }
    }
//      this block is currently not needed because everything has been consolidated
//    //send invitations to the notification gang
//    /**
//     * start trade api endpoint
//     * @param \Illuminate\Http\Request $request
//     * @return \Illuminate\Http\Response
//     */
//    public function notify(Request $request){
//        $validator = Validator::make($request->all(), [
//            'trade_id' => 'bail|required|exists:trade,id'
//        ]);
//
//        if ($validator->fails()){
//            return response()->json(['error'=>$validator->errors()], $this->errorStatus);
//        }else{
//            $input = $request->all();
//            foreach (Notification::where('trade_id', $input['trade_id'])->cursor() as $notificationGang){
//                $message = "Hi ".$notificationGang->first_name." ".$notificationGang->last_name.
//                    $this->stringHelper->notificationGangString();
//                try{
//                    $tradeRole = new TradeRole();
//                    $tradeRole->trade_id = $input['trade_id'];
//                    $tradeRole->user_email = $notificationGang->email;
//                    $tradeRole->user_role = 4;
//                    $tradeRole->save();
//                    $email = new SendTradeInvitation($message);
//                    Mail::to($notificationGang->email)->send($email);
//                }catch (Exception $exception){
//                    $success['error'] = "Some notification emails were not sent successfully";
//                }
//            }
//            $success['success'] = "Notification emails have been successfully sent";
//            return response()->json($success, $this-> successStatus);
//        }
//    }
//
//    //send invitations to the beneficiaries
//    /**
//     * start trade api endpoint
//     * @param \Illuminate\Http\Request $request
//     * @return \Illuminate\Http\Response
//     */
//    public function inviteBeneficiaries(Request $request){
//        $validator = Validator::make($request->all(), [
//            'trade_id' => 'bail|required|exists:trade,id'
//        ]);
//
//        if ($validator->fails()){
//            return response()->json(['error'=>$validator->errors()], $this->errorStatus);
//        }else{
//            $input = $request->all();
//            foreach (Tradebeneficiary::where('trade_id', $input['trade_id'])->cursor() as $beneficiary){
//                try {
//                    $tradeRole = new TradeRole();
//                    $tradeRole->trade_id = $input['trade_id'];
//                    $tradeRole->user_email = $beneficiary->email;
//                    $tradeRole->user_role = 5;
//                    $tradeRole->save();
//                    $email = new SendTradeInvitation($this->stringHelper->beneficiaryString());
//                    Mail::to($beneficiary->email)->send($email);
//                }catch (Exception $exception){
//                    $success['error'] = "Some beneficiary emails were not sent successfully";
//                }
//            }
//            $success['success'] = "Beneficiary invites have been successfully sent";
//            return response()->json($success, $this-> successStatus);
//        }
//    }

}

<?php

namespace App\Http\Controllers\API;

use App\Helper\GeneralHelper;
use App\Helper\StringHelper;
use App\Models\Bank;
use App\Models\Bankaccounttype;
use App\Models\Companycategory;
use App\Models\Identificationtype;
use App\Models\Occupation;
use App\Models\TradeRole;
use App\Models\User;
use App\Models\UserImage;
use App\Models\UsersOccupation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use App\Jobs\SendVerificationEmail;
use Mockery\Exception;
use Psy\Util\Str;

class RegisterController extends Controller
{
    public $successStatus = 200;

    public $errorStatus = 400;

    /**
     * Register api
     *@param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request){
       $validator = Validator::make($request->all(), [
           'email' => 'bail|required|email|unique:users,email|max:120',
           'password' => 'required',
           'confirm_password' => 'required|same:password'
       ]);

       if ($validator->fails()){
           $errors = $validator->errors();
           $array=[];
           array_push($array,$errors->first('email'));
           array_push($array,$errors->first('password'));
           array_push($array,$errors->first('confirm_password'));
           $error['error'] = implode($array);
           return response()->json($error, $this->errorStatus);
       }else{
           $input = $request->all();
           $input['password'] = Hash::make($input['password']);
           $input['email_token'] = $input['email']."Ghana";
           $user = User::create($input);
           //handling it globally
//           event(new Registered($user));
//           $this->dispatch(new SendVerificationEmail($user));
           $success['token'] = $user->createToken('Safe Transact'.$input['email'])->accessToken;
           $success['token_expiry_date'] = now()->addYear(1)->toDateString();
           $success['user'] = $user;
           $tradeRole = TradeRole::where('user_email', $input['email'])->with(
               'userTradeRole'
           )->get();
           if ($tradeRole->count() > 0){
               $success['user_trade_roles'] = $tradeRole;
           }

           return response()->json($success, $this->successStatus);
       }
    }

    /**
     * Resend verification email
     *
     */
    public function sendVerificationEmail(){
        $user = Auth::user();
        $this->dispatch(new SendVerificationEmail($user));
    }

    /**
     * Verify client email token
     *
     * @param $token
     * @return \Illuminate\Http\Response
     */
    public function verify($token){
        $user = User::where('email_token', $token)->first();
//        print_r($user->count());
//        die();
        if ($user->count() > 0){
            if ($user->email_confirmed == true){
                return view('email.emailfail',
                    ['text'=> "You have already confirmed this email address"]);
            }else{
                $user->email_confirmed = true;
                $user->save();
                if ($user->save()){
                    return view('email.emailverified', ['user'=> $user]);
                }else{
                    return view('email.emailfail',
                        ['text'=> "Email couldn't be confirmed"]);
                }
            }
        }else{
            return view('email.emailfail',
                ['text'=> "This email address does not exist in our records."]);
        }
    }

    /**
     * upload image api
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request){
        $validator = Validator::make($request->all(), [
            'image_byte_array'       => 'required'
        ]);
        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('image_byte_array'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            try{
                $user = Auth::user();
                $input = $request->all();
                $userOldImage = User::find($user->id)->image;
                if ($userOldImage != null){
                    $userOldImage->image_byte_array = $input['image_byte_array'];
                    $userOldImage->save();
                }else{
                    $input['image_name'] = "profile_pic_".$user->id."_".$user->first_name;
                    $input['user_id'] = $user->id;
                    UserImage::create($input);
                }
                $userImage = User::find($user->id)->image;
                $success['image_byte_array'] = $userImage->image_byte_array;
                return response()->json($success, $this->successStatus);
            }catch (Exception $exception){
                $error['error'] = "The image couldn't be uploaded. Please try again later";
                return response()->json($error, $this->errorStatus);
            }
        }
    }

    /**
     * update account api
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function completeAccount(Request $request){
        $validator = Validator::make($request->all(), [
            'first_name' => 'bail|sometimes|required|max:100',
            'last_name' => 'bail|sometimes|required|max:100',
            'mobile_dialing_code' => 'bail|sometimes|required|max:10',
            'mobile_number' => 'bail|sometimes|required|max:12',
            'home_dialing_code' => 'bail|sometimes|nullable|max:10',
            'home_number' => 'bail|sometimes|nullable|max:12',
            'identification' => 'bail|sometimes|required|exists:identificationtype,identification_type',
            'id_number' => 'bail|sometimes|required|max:20',
            'citizenship' => 'bail|sometimes|required|max:70',
            'occupation' => 'bail|sometimes|required|exists:occupations,occupation',
            'company_name' => 'bail|sometimes|nullable|max:100',
            'tax_identification_number' => 'bail|sometimes|nullable|max:20',
            'company_registration' => 'bail|sometimes|nullable|max:40',
            'company' => 'bail|sometimes|nullable|exists:companycategories,category',
            'bill_address_line_1' => 'bail|sometimes|nullable|max:150',
            'bill_address_line_2' => 'bail|sometimes|nullable|max:150',
            'bill_address_suburb' => 'bail|sometimes|nullable|max:100',
            'bill_address_city' => 'bail|sometimes|nullable|max:100',
            'bill_address_region' => 'bail|sometimes|nullable|max:100',
            'bill_address_country' => 'bail|sometimes|nullable|max:70',
            'bill_address_postcode' => 'bail|sometimes|nullable|max:50',
            'digital_address' => 'bail|sometimes|nullable|max:40',
            'bank_name' => 'bail|nullable|sometimes|exists:banks,bank_name',
            'account_number' => 'bail|sometimes|nullable|max:50',
            'account_name' => 'bail|sometimes|nullable|max:250',
            'bank_branch' => 'bail|sometimes|nullable|max:100',
            'account' => 'bail|sometimes|nullable|exists:bankaccounttypes,account_type',
            'device_id' => 'bail|sometimes|nullable|max:100'
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('first_name'));
            array_push($array,$errors->first('last_name'));
            array_push($array,$errors->first('mobile_dialing_code'));
            array_push($array,$errors->first('mobile_number'));
            array_push($array,$errors->first('home_dialing_code'));
            array_push($array,$errors->first('home_number'));
            array_push($array,$errors->first('identification'));
            array_push($array,$errors->first('id_number'));
            array_push($array,$errors->first('occupation'));
            array_push($array,$errors->first('citizenship'));
            array_push($array,$errors->first('company_name'));
            array_push($array,$errors->first('tax_identification_number'));
            array_push($array,$errors->first('company_registration'));
            array_push($array,$errors->first('company'));
            array_push($array,$errors->first('bill_address_line_1'));
            array_push($array,$errors->first('bill_address_line_2'));
            array_push($array,$errors->first('bill_address_suburb'));
            array_push($array,$errors->first('bill_address_city'));
            array_push($array,$errors->first('bill_address_region'));
            array_push($array,$errors->first('bill_address_country'));
            array_push($array,$errors->first('bill_address_postcode'));
            array_push($array,$errors->first('digital_address'));
            array_push($array,$errors->first('bank_name'));
            array_push($array,$errors->first('account_number'));
            array_push($array,$errors->first('account_name'));
            array_push($array,$errors->first('bank_branch'));
            array_push($array,$errors->first('account'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $user = Auth::user();
            $input = $request->all();
            $stringHelper = new StringHelper();
            $generalHelper =  new GeneralHelper();
            $generalHelper->setAccountCompleted($user->id);

            $user->fill($input);

            if (array_key_exists('occupation', $input)){
                $occupation = Occupation::where('occupation', $input['occupation'])->first();
                $currentOccupation = UsersOccupation::where('user_id', $user->id)->first();
                if ($currentOccupation->count() > 0){
                    $currentOccupation->occupation = $occupation->id;
                    $currentOccupation->save();
                }else{
                    UsersOccupation::create([
                        'user_id' => $user->id,
                        'user_occupation' => $occupation->id
                    ]);
                }
            }

            if (array_key_exists('bank_name', $input)){
                if (!$stringHelper->isNullOrEmpty($input['bank_name'])){
                    $bank = Bank::where('bank_name', $input['bank_name'])->first();
                    $user->bank()->associate($bank);
                }
            }

            if (array_key_exists('identification', $input)){
                if (!$stringHelper->isNullOrEmpty($input['identification'])){
                    $identification = Identificationtype::where('identification_type', $input['identification'])->first();
                    $user->identificationTypeUsed()->associate($identification);
                }
            }

            if (array_key_exists('company', $input)){
                if (!$stringHelper->isNullOrEmpty($input['company'])){
                    $company = Companycategory::where('category', $input['company'])->first();
                    $user->companyCategory()->associate($company);
                }
            }

            if (array_key_exists('account', $input)){
                if (!$stringHelper->isNullOrEmpty($input['account'])){
                    $account = Bankaccounttype::where('account_type', $input['account'])->first();
                    $user->bankAccountType()->associate($account);
                }
            }

            $user->save();
            //create JSON holder for the response
//            $sentUser = DB::table('users')->find($user->id);
            $success['user'] = $user;

            $userJob = UsersOccupation::where('user_id', $user->id)->first();
            if ($userJob != null){
                $success['user_occupation'] = $userJob->occupation;
            }

//            $editedUser['identification_type'] = $editedUser['identification_type']['id'];
//
//            $success['user'] = $editedUser;

            return response()->json($success, $this-> successStatus);
        }
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Helper\NotificationHelper;
use App\Models\Trade;
use App\Models\TradeRole;
use App\Models\User;
use App\Models\UserImage;
use App\Models\UsersOccupation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if (Auth::attempt(['email' => \request('email'), 'password' => \request('password')])){
            $user = Auth::user();
            $success['token'] = $user->createToken('Safe Transact'.\request('email').now()->toDateString())->accessToken;
            $success['token_expiry_date'] = now()->addMonths(6)->toDateTimeString();
            $success['user'] = $user->with([
                'identificationTypeUsed',
                'bank',
                'bankAccountType',
                'companyCategory'
            ])->first();
            $tradeRole = TradeRole::where('user_email', $user->email)->with(
                'userTradeRole'
            )->get();
            if ($tradeRole->count() > 0){
                $success['user_trade_roles'] = $tradeRole;
            }
            $userImage = User::find($user->id)->image;
            if ($userImage != null){
                $success['image'] = $userImage['image_byte_array'];
            }

            $userJob = UsersOccupation::where('user_id',  $user->id)->first();
            if ($userJob != null){
                $success['occupation'] = $userJob->occupation;
            }


            return response()->json($success, $this->successStatus);
        }else{
            $error['error']= 'Unauthorised';
            return response()->json($error, 401);
        }
    }

    public function forgotPassword(){

    }


    public function testNotify(){
//        $helper = new NotificationHelper();
//        $helper->sendNotification("hello", "Your API is working!", "Congratulations");
//        $userRole = TradeRole::where('trade_id', 1)->first()->userTradeRole->trade_role;
        $array=[];
        array_push($array,"Seller email was not sent");
        array_push($array,"Seller email was not sent");
        array_push($array,"Seller email was not sent");
        array_push($array,"Seller email was not sent");
        array_push($array,"Seller email was not sent");
        return response()->json($array, $this->successStatus);
    }
}

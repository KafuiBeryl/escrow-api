<?php

namespace App\Http\Controllers\API;

use App\Helper\NotificationHelper;
use App\Models\Trade;
use App\Models\Tradepayment;
use App\Helper\PaymentHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use http\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{
    private $paymentHelper;

    private $successStatus = 200;

    private $errorStatus = 400;

    private $guzzleClient;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->paymentHelper = new PaymentHelper();
        $this->guzzleClient = new Client(['base_uri' => env('PAY_SWITCH_URL')]);
    }

    //pay trade amount via mobile money
    /**
     * pay trade amount via mobile money
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function mobileMoneyPay(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id',
            'description' => 'bail|required|max:100',
            'subscriber_number' => 'bail|required|max:10',
            'network' => 'bail|required|max:10',
            'voucher_code' => 'bail|sometimes|required|max:20'
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            array_push($array,$errors->first('description'));
            array_push($array,$errors->first('subscriber_number'));
            array_push($array,$errors->first('network'));
            array_push($array,$errors->first('voucher_code'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $input = $request->all();
            $trade = Trade::find($input['trade_id']);
            $user = Auth::user();
            //set trade process id to "Funds Deposited"
//            $trade->tradeprocess_id = 5;
            $trade->save();
            $amount = $this->paymentHelper->formatAmount($trade->trade_amount);
            $transaction_id = $this->paymentHelper->createTransactionId($trade->id);

            switch ($input['network']){
                case 'Vodafone':
                    $input['network'] = "VDF";
                    break;
                case 'MTN':
                    $input['network'] = "MTN";
                    break;
                case 'Airtel':
                    $input['network'] = "ATL";
                    break;
                case 'Tigo':
                    $input['network'] = "TGO";
                    break;
            }

            $header_token = base64_encode(env('PAY_SWITCH_API_USER').':'.env('PAY_SWITCH_API_KEY'));
            $merchantId = env('PAY_SWITCH_MERCHANT_ID');

            if ($input['network'] == "VDF"){
                $body = '{
                    "amount" : "'.$amount.'",
                    "processing_code" : "000200",
                    "transaction_id" : "'.$transaction_id.'",
                    "desc" : "'.$input['description'].'",
                    "merchant_id" : "'.$merchantId.'",
                    "subscriber_number" : "'.$input['subscriber_number'].'",
                    "r-switch" : "'.$input['network'].'",
                    "voucher_code" : "'.$input['voucher_code'].'"
                }';
            }else{
                $body = '{
                    "amount" : "'.$amount.'",
                    "processing_code" : "000200",
                    "transaction_id" : "'.$transaction_id.'",
                    "desc" : "'.$input['description'].'",
                    "merchant_id" : "'.$merchantId.'",
                    "subscriber_number" : "'.$input['subscriber_number'].'",
                    "r-switch" : "'.$input['network'].'"
                }';
            }

            try{
                $response = $this->guzzleClient->post("", [
                    'headers' => [
                        'accept' => 'application/json',
                        'content-type' => 'application/json; charset=UTF8',
                        'timeout' => 5000,
                        'authorization' => 'Basic '. $header_token,
                    ],
                    'body' => $body
                ]);

                if ($response->getStatusCode() == 200){
                    $notificationHelper = new NotificationHelper();
                    $notificationHelper->sendNotification($input['trade_id'], "Funds deposited", "The funds for the trade ".$trade->trade_name." have been deposited successfully");
                    $daBody = json_decode($response->getBody(), true);
                    $responseBody = $daBody['status']." ".$daBody['reason'];

                    if ($daBody['status'] == "approved"){
                        Tradepayment::create([
                            'trade_id' => $trade->id,
                            'amount_paid' => $amount,
                            'user_id' => $user->id,
                            'isSuccess' => true,
                            'payment_mode' => 'Mobile Money',
                            'status' => $daBody['status'],
                            'reason' => $daBody['reason'],
                            'transaction_id' => $transaction_id
                        ]);
                        //set trade process id to "Funds Received and cleared"
                        $trade->tradeprocess_id = 6;
                    }else{
                        Tradepayment::create([
                            'trade_id' => $trade->id,
                            'amount_paid' => $amount,
                            'user_id' => $user->id,
                            'isSuccess' => false,
                            'payment_mode' => 'Mobile Money',
                            'status' => $daBody['status'],
                            'reason' => $daBody['reason'],
                            'transaction_id' => $transaction_id
                        ]);
                        //set trade process id to "Funds Received and cleared"
                        $trade->tradeprocess_id = 4;
                    }

                }else{
                    Tradepayment::create([
                        'trade_id' => $trade->id,
                        'amount_paid' => $amount,
                        'user_id' => $user->id,
                        'isSuccess' => false,
                        'payment_mode' => 'Mobile Money',
                        'status' => 'failed',
                        'reason' => 'Http bad request',
                        'transaction_id' => $transaction_id
                    ]);
                    //set trade process id to "Funds Received and cleared"
                    $trade->tradeprocess_id = 4;
                    $responseBody = json_decode($response->getReasonPhrase());
                }
            }catch (RequestException $requestException){
                $responseBody = $requestException->getMessage();
            }catch (Exception $exception){
                $responseBody = $exception->getMessage();
            }

            $success['message'] = $responseBody;
            return response()->json($success, $this-> successStatus);
        }
    }

    //pay trade amount via bank card
    /**
     * pay trade amount via mobile money
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function bankCardPay(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id',
            'description' => 'bail|required|max:100',
            'card_number' => 'bail|number|required|max:20',
            'card_type' => 'bail|required|max:10',
            'cvv' => 'bail|required|max:4',
            'exp_month' => 'bail|required|max:2',
            'exp_year' => 'bail|required|max:2'
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            array_push($array,$errors->first('description'));
            array_push($array,$errors->first('card_number'));
            array_push($array,$errors->first('card_type'));
            array_push($array,$errors->first('cvv'));
            array_push($array,$errors->first('exp_month'));
            array_push($array,$errors->first('exp_year'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $input = $request->all();
            $trade = Trade::find($input['trade_id']);
            $user = Auth::user();
            //set trade process id to "Funds Deposited"
//            $trade->tradeprocess_id = 5;
            $trade->save();
            $amount = $this->paymentHelper->formatAmount($trade->trade_amount);
            $transaction_id = $this->paymentHelper->createTransactionId($trade->id);

            switch ($input['card_type']){
                case 'MasterCard':
                    $input['card_type'] = "MAS";
                    break;
                case 'VISA':
                    $input['card_type'] = "VIS";
                    break;
            }

            $header_token = base64_encode(env('PAY_SWITCH_API_USER').':'.env('PAY_SWITCH_API_KEY'));
            $merchantId = env('PAY_SWITCH_MERCHANT_ID');

            $body = '{
                    "amount" : "'.$amount.'",
                    "processing_code" : "000100",
                    "transaction_id" : "'.$transaction_id.'",
                    "desc" : "'.$input['description'].'",
                    "merchant_id" : "'.$merchantId.'",
                    "r-switch" : "'.$input['card_type'].'",
                    "cvv" : "'.$input['cvv'].'",
                    "pan" : "'.$input['card_number'].'",
                    "exp_month" : "'.$input['exp_month'].'",
                    "exp_year" : "'.$input['exp_year'].'",
                    "3d_url_response" : "'.env('APP_URL').'/api/3d/url"
                }';

            try{
                $response = $this->guzzleClient->post("", [
                    'headers' => [
                        'accept' => 'application/json',
                        'content-type' => 'application/json; charset=UTF8',
                        'timeout' => 5000,
                        'authorization' => 'Basic '. $header_token,
                    ],
                    'body' => $body
                ]);

                if ($response->getStatusCode() == 200){
                    $daBody = json_decode($response->getBody(), true);
                    $responseBody = $daBody['status']." ".$daBody['reason'];

                    if ($daBody['status'] == "approved"){
                        Tradepayment::create([
                            'trade_id' => $trade->id,
                            'amount_paid' => $amount,
                            'user_id' => $user->id,
                            'isSuccess' => true,
                            'payment_mode' => 'Card Payment',
                            'status' => $daBody['status'],
                            'reason' => $daBody['reason'],
                            'transaction_id' => $transaction_id
                        ]);
                        //set trade process id to "Funds Received and cleared"
                        $trade->tradeprocess_id = 6;
                    }else{
                        Tradepayment::create([
                            'trade_id' => $trade->id,
                            'amount_paid' => $amount,
                            'user_id' => $user->id,
                            'isSuccess' => false,
                            'payment_mode' => 'Card Payment',
                            'status' => $daBody['status'],
                            'reason' => $daBody['reason'],
                            'transaction_id' => $transaction_id
                        ]);
                        //set trade process id to "Funds Received and cleared"
                        $trade->tradeprocess_id = 4;
                    }

                }else{
                    Tradepayment::create([
                        'trade_id' => $trade->id,
                        'amount_paid' => $amount,
                        'user_id' => $user->id,
                        'isSuccess' => false,
                        'payment_mode' => 'Card Payment',
                        'status' => 'failed',
                        'reason' => 'Http bad request',
                        'transaction_id' => $transaction_id
                    ]);
                    //set trade process id to "Funds Received and cleared"
                    $trade->tradeprocess_id = 4;
                    $responseBody = json_decode($response->getReasonPhrase());
                }
            }catch (RequestException $requestException){
                $responseBody = $requestException->getMessage();
            }catch (Exception $exception){
                $responseBody = $exception->getMessage();
            }

            $success['message'] = $responseBody;
            return response()->json($success, $this-> successStatus);
        }
    }
}

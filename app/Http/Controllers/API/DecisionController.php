<?php

namespace App\Http\Controllers\API;

use App\Helper\GeneralHelper;
use App\Models\Trade;
use App\Models\TradeRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class DecisionController extends Controller
{
    public $successStatus = 200;

    public $errorStatus = 400;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * implement trade decision
     *@param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function implementDecision(Request $request){
        $validator = Validator::make($request->all(), [
            'trade_id' => 'bail|required|exists:trade,id',
            'decision' => [
                'required',
                Rule::in(['Accept', 'Decline', 'Cancel'])
            ]
        ]);

        if ($validator->fails()){
            $errors = $validator->errors();
            $array=[];
            array_push($array,$errors->first('trade_id'));
            array_push($array,$errors->first('decision'));
            $error['error'] = implode($array);
            return response()->json($error, $this->errorStatus);
        }else{
            $input = $request->all();
            $trade = Trade::find($input['trade_id']);
            $userRole = TradeRole::where('trade_id', $trade->id)->first()->userTradeRole->trade_role;
            $generalHelper = new GeneralHelper();
            //check what decision is and implement it
            if ($input['decision'] == "Accept"){
                $generalHelper->addTradeAcceptance($userRole, $trade->id);
            }elseif ($input['decision'] == "Decline"){
                $generalHelper->addTradeDeclined($userRole, $trade->id);
            }elseif ($input['decision'] == "Cancel"){
                $trade->trade_status = 2;
            }

            //set current trade process if all participants have accepted the trade
            if ($generalHelper->checkIfTradeIsAccepted($trade->id) == true){
                $trade->tradeprocess_id = 4;
            }else{
                $trade->tradeprocess_id = 11;
            }
            $trade->save();

            $userTrades = $trade->with([
                'standardPayeeAllocation',
                'feeType',
                'country',
                'adminPayeeAllocation',
                'tradeStatus',
                'userTradeRole',
                'tradeStep',
                'tradeIndustry'
            ])->first();
            $success['trade'] = $userTrades;
            return response()->json($success, $this-> successStatus);
        }
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Models\Adminpayeeallocation;
use App\Models\Bank;
use App\Models\Bankaccounttype;
use App\Models\Beneficiarytype;
use App\Models\Companycategory;
use App\Models\Feeschedule;
use App\Models\Feetype;
use App\Models\Identificationtype;
use App\Models\Industry;
use App\Models\Occupation;
use App\Models\Standardpayeeallocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public $successStatus = 200;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Get miscellaneous data that will be used in the app.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMiscData(){
        $success['banks'] = Bank::all();
        $success['industries'] = Industry::all();
        $success['company_type'] = Companycategory::all();
        $success['feeSchedule'] = Feeschedule::all();
        $success['bankAccountTypes'] = Bankaccounttype::all();
        $success['identificationTypes'] = Identificationtype::all();
        $success['feeType'] = Feetype::all();
        $success['standardFeeAllocation'] = Standardpayeeallocation::all();
        $success['adminFeeAllocation'] = Adminpayeeallocation::all();
        $success['beneficiaryType'] = Beneficiarytype::all();
        $success['occupations'] = Occupation::orderBy('rank', 'asc')->get();
        return response()->json($success, $this->successStatus);
    }
}

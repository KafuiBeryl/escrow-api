<?php

namespace App\Jobs;

use App\Models\Trade;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TradeDeliveryWaitTime implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $trade;
    protected $user;
    /**
     * Create a new job instance.
     * @param Trade $trade
     * @param User $user
     * @return void
     */
    public function __construct(Trade $trade, User $user)
    {
        $this->trade = $trade;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}

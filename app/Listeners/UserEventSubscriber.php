<?php

namespace App\Listeners;

use App\Jobs\SendVerificationEmail;
use App\Models\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserEventSubscriber
{

    /**
     * Handle user register events.
     * @param  \Illuminate\Auth\Events\Registered  $event
     */
    public function onRegister($event){
        $user = User::find($event->user->id);
        SendVerificationEmail::dispatch($user);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events){
        $events->listen(
            'Illuminate\Auth\Events\Registered',
            'App\Listeners\UserEventSubscriber@onRegister'
        );
    }
}

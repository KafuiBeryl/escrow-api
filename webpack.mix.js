let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//     .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/app.js', 'public/js');
// mix.styles([
//         'resources/assets/css/AddBeneficiary.css',
//         'resources/assets/css/bootstrap.min.css',
//         'resources/assets/css/Delivery.css',
//         'resources/assets/css/DescribeTrade.css',
//         'resources/assets/css/DescribeTradeAgent.css',
//         'resources/assets/css/LogIn.css',
//         'resources/assets/css/main.css',
//         'resources/assets/css/NewTrade.css',
//         'resources/assets/css/Notification.css',
//         'resources/assets/css/ProgressPayment.css',
//         'resources/assets/css/responsive.css',
//         'resources/assets/css/set_trade_parties.css',
//         'resources/assets/css/SignUp.css',
//         'resources/assets/css/UploadDocument.css',
//         'resources/assets/css/dropzone/dropzone.css',
//         'resources/assets/css/Responsive/AddBeneficiary_responsive.css'
//     ], 'public/css/style.css');

<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//create a new user for registration
Route::post('register', 'API\RegisterController@register');

Route::post('login', 'API\LoginController@login');

Route::group(['middleware' => 'auth:api'], function(){
    //update account info
    Route::post('account', 'API\RegisterController@completeAccount');

    //resend verification email
    Route::post('resend/email', 'API\RegisterController@sendVerificationEmail');

    //update account info
    Route::post('account/image', 'API\RegisterController@uploadImage');
});

//get miscellaneous data
Route::get('home/misc/get', 'API\HomeController@getMiscData');

//initialize trade
Route::post('trade/initialize', 'API\TransactionController@startBasicTrade');

//edit trade
Route::post('trade/edit', 'API\TransactionController@editTransaction');

//add trade extras to trade eg. notification gang
Route::post('trade/initialize/extras', 'API\TransactionController@addTradeExtras');

//edit trade extras to trade eg. notification gang
Route::post('trade/edit/extras', 'API\TransactionController@editTradeExtras');

//get all user initiated trades
Route::get('trade/get/all', 'API\TransactionController@getInvolvedTrades');

//get all trade extras by id
Route::post('trade/get/extras', 'API\TransactionController@getTradeExtras');

//get trade by id
Route::post('trade/get', 'API\TransactionController@getTradeById');

//get all trades the user has been invited to.
Route::get('trade/get/invited', 'API\TransactionController@getInviteeTrades');

//Invite all active parties to the transaction
Route::post('invite/send', 'API\InvitationController@sendInvitation');

//Invite notification gang to the transaction
//Route::post('invite/notification/send', 'API\InvitationController@notify');

//Invite beneficiaries to the transaction
//Route::post('invite/beneficiary/send', 'API\InvitationController@inviteBeneficiaries');

//trade decision. Send user's decision
Route::post('trade/user/decision', 'API\DecisionController@implementDecision');

//pay via mobile money
Route::post('make/payment/mobile', 'API\PaymentController@mobileMoneyPay');

//pay via card
Route::post('make/payment/card', 'API\PaymentController@bankCardPay');

//3d url
Route::post('3d/url', 'API\CallbackController@index');

//chat : send message to all concerned parties
Route::post('negotiate/chat/send', 'API\ChatController@sendMessage');

//test notification
//Route::get('test_notify', 'API\LoginController@testNotify');
